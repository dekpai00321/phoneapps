/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.model;

import java.util.Date;
import java.util.Vector;

/**
 * 
 * NewsItem object represents a news article that will be or has been posted to
 * Indybay.org
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class NewsItem {

	private static final long serialVersionUID = 6193104687131564031L;;

	/**
	 * attachments which are images, video, and perhaps at some point audio
	 */
	private Vector attachments = null;

	/**
	 * the displayed name of the person posting the newsitem
	 */
	private String author = null;

	/**
	 * should the email be displayed or not
	 */
	private boolean displayEmail = false;

	/**
	 * the email of the person posting the newsitem
	 */
	private String email = null;

	/**
	 * the id of the newsitem once it is posted
	 */
	private Integer id = null;

	/**
	 * the url of the newsitem once it is posted
	 */
	private String postedURL = null;

	/**
	 * the time when a newsitem was published
	 */
	private Date publishDate = null;

	/**
	 * The region where the news event took place
	 */
	private int regionId = 0;

	/**
	 * A link displayed after the newsitem and each attachment
	 */
	private String relatedLink = null;

	/**
	 * Summary of news story
	 */
	private String summary = null;

	/**
	 * Text displayed after summary
	 */
	private String text = null;

	/**
	 * Title of the newsitem
	 */
	private String title = null;

	/**
	 * topic of the newsitem
	 */
	private int topicId = 0;

	/**
	 * needed for Serialized load
	 */
	public NewsItem() {
	}

	/**
	 * constructor requires preferences to allow initial values to be set
	 * correctly
	 * 
	 * @param preferences
	 */
	public NewsItem(final IndyPreferences preferences) {
		this.loadNewsItemFromPrefs(preferences);
	}

	/**
	 * @return
	 */
	public Vector getAttachments() {
		if (this.attachments == null) {
			this.attachments = new Vector(2);
		}
		return this.attachments;
	}

	/**
	 * @return
	 */
	public String getAuthor() {
		if (this.author == null) {
			this.author = "";
		}
		return this.author;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		if (this.email == null) {
			this.email = "";
		}
		return this.email;
	}

	/**
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @return
	 */
	public String getPostedURL() {
		return this.postedURL;
	}

	/**
	 * @return
	 */
	public Date getPublishDate() {
		return this.publishDate;
	}

	/**
	 * @return
	 */
	public int getRegionId() {
		return this.regionId;
	}

	/**
	 * @return
	 */
	public String getRelatedLink() {
		if (this.relatedLink == null) {
			this.relatedLink = "";
		}
		return this.relatedLink;
	}

	/**
	 * @return
	 */
	public String getSummary() {
		if (this.summary == null) {
			this.summary = "";
		}
		return this.summary;
	}

	/**
	 * @return
	 */
	public String getText() {
		if (this.text == null) {
			this.text = "";
		}
		return this.text;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		if (this.title == null) {
			this.title = "";
		}
		return this.title;
	}

	/**
	 * @return
	 */
	public int getTopicId() {
		return this.topicId;
	}

	/**
	 * @return
	 */
	public boolean isDisplayEmail() {
		return this.displayEmail;
	}

	/**
	 * @param newsItemAsString
	 */
	public void loadFromString(String newsItemAsString) {
		int i1 = 0;
		int i2 = 0;
		int k = 0;
		String nextString = null;
		String nextKey = null;
		newsItemAsString = "\n" + newsItemAsString;
		while (i2 > -1) {
			i2 = newsItemAsString.indexOf("\n", i1 + 1);
			if (i2 > 0) {
				nextString = newsItemAsString.substring(i1 + 1, i2);
			} else {
				nextString = newsItemAsString.substring(i1 + 1);
				i2 = -1;
			}
			i1 = i2;
			k = nextString.indexOf(":");
			nextKey = null;
			if (k > 0) {
				nextKey = nextString.substring(0, k);
			}
			if (nextKey != null) {
				if (nextKey.equals("title")) {
					this.title = nextString.substring(nextKey.length() + 1);
					continue;
				}
				if (nextKey.equals("author")) {
					this.author = nextString.substring(nextKey.length() + 1);
					continue;
				}
				if (nextKey.equals("summary")) {
					this.summary = nextString.substring(nextKey.length() + 1)
							.replace('\t', '\n');
					continue;
				}
				if (nextKey.equals("text")) {
					this.text = nextString.substring(nextKey.length() + 1)
							.replace('\t', '\n');
					continue;
				}
				if (nextKey.equals("attachments")) {
					final String tempAttachmentString = nextString
							.substring(nextKey.length() + 1);
					this.loadAttachementsFromPaths(tempAttachmentString);
					continue;
				}
			}

		}
	}

	/**
	 * Reset fields and erase attachments based off preferences
	 * 
	 * @param preferences
	 */
	public void reset(final IndyPreferences preferences) {
		if (preferences.isResetAuthor()) {
			this.setAuthor(preferences.getDefaultAuthor());
		}
		if (preferences.isResetTitle()) {
			this.setTitle(preferences.getDefaultTitle());
		}
		if (preferences.isResetSummary()) {
			this.setSummary(preferences.getDefaultSummary());
		}
		if (preferences.isResetText()) {
			this.setText(preferences.getDefaultText());
		}

		this.setEmail(preferences.getDefaultEmail());
		this.setRelatedLink(preferences.getDefaultRelatedLink());
		this.setAttachments(new Vector());
	}

	/**
	 * @param attachments
	 */
	public void setAttachments(final Vector attachments) {
		this.attachments = attachments;
	}

	/**
	 * @param author
	 */
	public void setAuthor(final String author) {
		this.author = author;
	}

	/**
	 * @param displayEmail
	 */
	public void setDisplayEmail(final boolean displayEmail) {
		this.displayEmail = displayEmail;
	}

	/**
	 * @param email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @param id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @param postedURL
	 */
	public void setPostedURL(final String postedURL) {
		this.postedURL = postedURL;
	}

	/**
	 * @param publishDate
	 */
	public void setPublishDate(final Date publishDate) {
		this.publishDate = publishDate;
	}

	/**
	 * @param regionId
	 */
	public void setRegionId(final int regionId) {
		this.regionId = regionId;
	}

	/**
	 * @param relatedLink
	 */
	public void setRelatedLink(final String relatedLink) {
		this.relatedLink = relatedLink;
	}

	/**
	 * @param summary
	 */
	public void setSummary(final String summary) {
		this.summary = summary;
	}

	/**
	 * @param text
	 */
	public void setText(final String text) {
		this.text = text;
	}

	/**
	 * @param title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * @param topicId
	 */
	public void setTopicId(final int topicId) {
		this.topicId = topicId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("title:");
		buffer.append(this.getTitle());
		buffer.append("\n");
		buffer.append("author:");
		buffer.append(this.getAuthor());
		buffer.append("\n");
		buffer.append("summary:");
		buffer.append(this.getSummary().replace('\n', '\t'));
		buffer.append("\n");
		buffer.append("attachments:");
		buffer.append(this.getDelimitedAttachmentPaths());
		buffer.append("\n");
		buffer.append("text:");
		buffer.append(this.getText().replace('\n', '\t'));
		return buffer.toString();
	}

	/**
	 * @return
	 */
	private String getDelimitedAttachmentPaths() {
		final StringBuffer buffer = new StringBuffer("");
		NewsItemAttachment nextAttachment = null;
		for (int i = 0; i < this.getAttachments().size(); i++) {
			nextAttachment = (NewsItemAttachment) this.getAttachments()
					.elementAt(i);
			buffer.append(nextAttachment.getFilePath());
			buffer.append(";");
		}
		return buffer.toString();

	}

	/**
	 * @param tempAttachmentString
	 */
	private void loadAttachementsFromPaths(final String tempAttachmentString) {
		this.getAttachments().removeAllElements();
		// cant user tokenizer due to j2me limitations
		int i = -1;
		int j = 0;
		NewsItemAttachment nextAttachment = null;
		String nextPath = null;
		while (j > -1) {
			j = tempAttachmentString.indexOf(";", i + 1);
			if (j > -1) {

				nextPath = tempAttachmentString.substring(i + 1, j);
				System.out.println("next path was " + nextPath);
				nextAttachment = new NewsItemAttachment();
				nextAttachment.setFilePath(nextPath);
				this.getAttachments().addElement(nextAttachment);

			}
			i = j;
		}

	}

	/**
	 * @param preferences
	 */
	private void loadNewsItemFromPrefs(final IndyPreferences preferences) {
		this.setAuthor(preferences.getDefaultAuthor());
		this.setTitle(preferences.getDefaultTitle());
		this.setSummary(preferences.getDefaultSummary());
		this.setText(preferences.getDefaultText());

		this.setEmail(preferences.getDefaultEmail());
		this.setRelatedLink(preferences.getDefaultRelatedLink());

		this.setAttachments(new Vector());
	}

}
