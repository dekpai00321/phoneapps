/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.model;

/**
 * An atachment on a newsitem containing an image, a video or perhaps at some
 * point audio
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class NewsItemAttachment {

	private static final long serialVersionUID = 3378509154606716501L;

	/**
	 * Is the attachment an image, video, etc...
	 */
	private int attachmentType = 0;

	/**
	 * file path on the client of the attachment so it can be streamed to server
	 * during a POST
	 */
	private String filePath = null;

	public int getAttachmentType() {
		return this.attachmentType;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setAttachmentType(final int attachmentType) {
		this.attachmentType = attachmentType;
	}

	public void setFilePath(final String filePath) {
		this.filePath = filePath;
	}

}
