/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.model;

/**
 * File containing configuration informating that can either be modified by end
 * users or might need to be in the future
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class IndyPreferences {

	/**
	 * Default name for preference file
	 */
	public static final String PREFS_NAME = "IndybayAndroidPrefs";

	/**
	 * application key letting the server know which application is being used
	 * to post to the mobile publish page
	 */
	private String applicationKey = "at#Hqe_t342qtzz";

	/**
	 * base url that is being posted to
	 */
	private String baseURL = "www.indybay.org";

	/**
	 * 
	 */
	private String defaultAuthor = "Anonymous";

	/**
	 * 
	 */
	private String defaultEmail = "";

	/**
	 * 
	 */
	private String defaultRelatedLink = "";

	/**
	 * 
	 */
	private String defaultSummary;

	/**
	 * 
	 */
	private String defaultText = "Pictures were posted from my phone using IndybayAndroidPublisher.";

	/**
	 * 
	 */
	private String defaultTitle = "Photos From Protest";
	/**
	 * protocol to use before baseURL when accessing site (will normally be
	 * either http or https)
	 */
	private String httpProtocol = "http://";

	/**
	 * 
	 */
	private int maxFilesAllowed = 5;

	/**
	 * 
	 */
	private int maxUploadSizeAllowed = 30;

	/**
	 * 
	 */
	private String personalKey = null;

	/**
	 * url to use on top of baseURL to use during POSTing
	 */
	private String relativePublishPageURLPath = "/mobile/mpublish.php";

	/**
	 * 
	 */
	private boolean resetAuthor = false;
	/**
	 * 
	 */
	private boolean resetSummary = true;

	/**
	 * 
	 */
	private boolean resetText = true;

	/**
	 * 
	 */
	private boolean resetTitle = true;

	public String getApplicationKey() {
		return this.applicationKey;
	}

	public String getBaseURL() {
		return this.baseURL;
	}

	public String getDefaultAuthor() {
		return this.defaultAuthor;
	}

	public String getDefaultEmail() {
		return this.defaultEmail;
	}

	public String getDefaultRelatedLink() {
		return this.defaultRelatedLink;
	}

	public String getDefaultSummary() {
		return this.defaultSummary;
	}

	public String getDefaultText() {
		return this.defaultText;
	}

	public String getDefaultTitle() {
		return this.defaultTitle;
	}

	public String getHttpProtocol() {
		return this.httpProtocol;
	}

	public int getMaxFilesAllowed() {
		return this.maxFilesAllowed;
	}

	public int getMaxUploadSizeAllowed() {
		return this.maxUploadSizeAllowed;
	}

	/**
	 * generates a new unique key if one was not previously set
	 * 
	 * @return personal key
	 */
	public String getPersonalKey() {
		if ((this.personalKey == null) || this.personalKey.trim().equals("")) {
			this.personalKey = "PK"
					+ String.valueOf(System.currentTimeMillis());
		}
		return this.personalKey;
	}

	/**
	 * appended together string containing http protocol, baseurl and
	 * relativePublishPageURLPath
	 * 
	 * @return full url String for publish page to post to
	 */
	public String getPublishPageURL() {
		return this.httpProtocol + this.baseURL
				+ this.relativePublishPageURLPath;
	}

	public String getRelativePublishPageURLPath() {
		return this.relativePublishPageURLPath;
	}

	public boolean isResetAuthor() {
		return this.resetAuthor;
	}

	public boolean isResetSummary() {
		return this.resetSummary;
	}

	public boolean isResetText() {
		return this.resetText;
	}

	public boolean isResetTitle() {
		return this.resetTitle;
	}

	public void setApplicationKey(final String applicationKey) {
		this.applicationKey = applicationKey;
	}

	public void setBaseURL(final String baseURL) {
		this.baseURL = baseURL;
	}

	public void setDefaultAuthor(final String defaultAuthor) {
		this.defaultAuthor = defaultAuthor;
	}

	public void setDefaultEmail(final String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	public void setDefaultRelatedLink(final String defaultRelatedLink) {
		this.defaultRelatedLink = defaultRelatedLink;
	}

	public void setDefaultSummary(final String defaultSummary) {
		this.defaultSummary = defaultSummary;
	}

	public void setDefaultText(final String defaultText) {
		this.defaultText = defaultText;
	}

	public void setDefaultTitle(final String defaultTitle) {
		this.defaultTitle = defaultTitle;
	}

	public void setHttpProtocol(final String httpProtocol) {
		this.httpProtocol = httpProtocol;
	}

	public void setMaxFilesAllowed(final int maxFilesAllowed) {
		this.maxFilesAllowed = maxFilesAllowed;
	}

	public void setMaxUploadSizeAllowed(final int maxUploadSizeAllowed) {
		this.maxUploadSizeAllowed = maxUploadSizeAllowed;
	}

	public void setPersonalKey(final String personalKey) {
		this.personalKey = personalKey;
	}

	public void setRelativePublishPageURLPath(
			final String relativePublishPageURLPath) {
		this.relativePublishPageURLPath = relativePublishPageURLPath;
	}

	public void setResetAuthor(final boolean resetAuthor) {
		this.resetAuthor = resetAuthor;
	}

	public void setResetSummary(final boolean resetSummary) {
		this.resetSummary = resetSummary;
	}

	public void setResetText(final boolean resetText) {
		this.resetText = resetText;
	}

	public void setResetTitle(final boolean resetTitle) {
		this.resetTitle = resetTitle;
	}

}
