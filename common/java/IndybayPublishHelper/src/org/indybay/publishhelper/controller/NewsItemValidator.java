/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.controller;

import java.util.Vector;

import org.indybay.publishhelper.model.NewsItem;

/**
 * Validation class that determines if the server will accept the NewsItem
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class NewsItemValidator {

	/**
	 * Validates a news item and returns null if it is valid or an ArrayList of
	 * validation problems if it is not valid
	 * 
	 * @param newsItem
	 * @return list of validation failures
	 */
	public static Vector validateNewsItem(final NewsItem newsItem) {
		final Vector validationProblems = new Vector();
		if ((newsItem.getTitle() == null)
				|| newsItem.getTitle().trim().equals("")) {
			validationProblems.addElement("Title Is Required");
		} else if (newsItem.getTitle().trim().length() < 8) {
			validationProblems
					.addElement("Title Must Be At Least 8 Characters In Length");
		}
		if ((newsItem.getAuthor() == null)
				|| newsItem.getAuthor().trim().equals("")) {
			validationProblems.addElement("Author Is Required");
		}
		if ((newsItem.getSummary() == null)
				|| newsItem.getSummary().trim().equals("")) {
			validationProblems.addElement("Summary Is Required");
		} else if (newsItem.getSummary().trim().length() < 8) {
			validationProblems
					.addElement("Summary Must Be At Least 8 Characters In Length");
		}
		if ((newsItem.getAttachments().size() == 0)
				&& ((newsItem.getText() == null) || newsItem.getSummary()
						.trim().equals(""))) {
			validationProblems.addElement("Text Is Required");
		} else if ((newsItem.getAttachments().size() == 0)
				&& (newsItem.getText().trim().length() < 12)) {
			validationProblems
					.addElement("Text Must Be At Least 12 Characters In Length");
		}
		return validationProblems;

	}
}
