/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.persistence;

import java.io.IOException;

import org.indybay.publishhelper.model.IndyPreferences;
import org.indybay.publishhelper.model.NewsItem;

/**
 * @author Zogren
 * 
 */
public abstract class NewsItemPersister {

	/**
	 * @return
	 */
	public abstract NewsItem load() throws IOException;

	/**
	 * Sets up a news item by either loading it from a cached file or setting up
	 * a new one with default values
	 * 
	 * @param filePath
	 * @param preferences
	 * @return newsItem
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */

	public NewsItem loadFromFileOrInitFromPrefs(
			final IndyPreferences preferences)

	{

		NewsItem newsItem = null;
		try {
			newsItem = this.load();
		} catch (final Exception ee) {
			ee.printStackTrace();
		}
		if (newsItem == null) {
			newsItem = new NewsItem(preferences);
		}
		return newsItem;

	}

	/**
	 * @param newsItem
	 */
	public abstract void save(NewsItem newsItem) throws IOException;

}
