/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Zogren
 * 
 */
public abstract class IOHelper {

	/**
	 * @param in
	 * @param os
	 * @throws IOException
	 */
	protected void copyFromInputToOutput(final InputStream in,
			final OutputStream os) throws IOException {
		int num = in.available();
		byte[] data = new byte[num];
		while (num > 0) {
			num = in.read(data);
			os.write(data);
			data = null;
			System.gc();
			if (num > 0) {
				data = new byte[num];
			}

		}
	}

	/**
	 * @param in
	 * @return
	 * @throws IOException
	 */
	protected String getStringFromInputStream(final InputStream in)
			throws IOException {
		int num = in.available();
		byte[] data = new byte[num];
		String str = "";
		while (num > 0) {
			in.read(data);
			str = str + new String(data);
			num = in.available();
			data = new byte[num];
		}
		in.close();
		return str;
	}

}
