package org.indybay.publishhelper.test;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author  Zogren for Indybay.org
 *
 */
public class PublishHelperTestSuite {

	/**
	 * @return
	 */
	public static Test suite() {
		final TestSuite suite = new TestSuite(
				"Test for org.indybay.publishhelper.test");
		// $JUnit-BEGIN$
		//suite.addTestSuite(NewsItemValidatorTest.class);
		//suite.addTestSuite(CaptchaSolverTest.class);
		suite.addTestSuite(NewsItemPublisherTest.class);
		// $JUnit-END$
		return suite;
	}

}
