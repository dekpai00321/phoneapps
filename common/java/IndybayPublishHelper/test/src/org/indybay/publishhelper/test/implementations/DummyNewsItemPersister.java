/**
 * 
 */
package org.indybay.publishhelper.test.implementations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.indybay.publishhelper.model.NewsItem;
import org.indybay.publishhelper.persistence.NewsItemPersister;


/**
 * @author Zogren
 *
 */
public class DummyNewsItemPersister extends NewsItemPersister{

	/**
	 * 
	 */
	private String newsItemFilePath = "currentNewsItem.ips";

	/**
	 * Sets up a news item by either loading it from a cached file or setting up
	 * a new one with default values
	 * 
	 * @param filePath
	 * @param preferences
	 * @return newsItem
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */

	public NewsItem load()

	{

		NewsItem newsItem = null;
		try {
			final File fc = new File(newsItemFilePath);

			if (fc.exists()) {
				try {
					String str = "";
					InputStream is = new FileInputStream(fc);
					byte[] data = new byte[is.available()];
					is.read(data);
					is.close();
					NewsItem newNewsItem = new NewsItem();
					str = new String(data);
					newNewsItem.loadFromString(str);
					newsItem=null;
					System.gc();
					newsItem=newNewsItem;

				} catch (final Exception ee) {
					ee.printStackTrace();
					fc.delete();
				}
			}
		} catch (final Exception ee) {
					ee.printStackTrace();

		}
		return newsItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.java.persistence.NewsItemPersister#save(org.indybay.java.
	 * model.NewsItem)
	 */
	public void save(NewsItem newsItem) throws IOException {

		File fc = new File(newsItemFilePath);
		if (fc.exists())
			fc.delete();
		
		OutputStream os = new FileOutputStream(fc);

		os.write(newsItem.toString().getBytes());
		os.close();

	}

}
