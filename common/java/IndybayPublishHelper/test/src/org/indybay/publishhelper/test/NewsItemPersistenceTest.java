package org.indybay.publishhelper.test;

import java.util.Vector;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.indybay.publishhelper.controller.NewsItemPublisher;
import org.indybay.publishhelper.model.IndyPreferences;
import org.indybay.publishhelper.model.NewsItem;
import org.indybay.publishhelper.model.NewsItemAttachment;
import org.indybay.publishhelper.test.implementations.DummyNewsItemPersister;
import org.indybay.publishhelper.test.implementations.DummyWebHelper;

/**
 * @author  Zogren for Indybay.org
 *
 */
public class NewsItemPersistenceTest extends TestCase {
	/**
	 * 
	 */
	public void testPostWithFiles() {
		try {
			IndyPreferences preferences= new IndyPreferences();
			//preferences.setBaseURL("www.indybay.org");
			
			
			final NewsItem newNewsItem = new NewsItem(preferences);
			newNewsItem.setTitle("12345678");
			newNewsItem.setAuthor("Test author 222");
			newNewsItem.setEmail("Test email a@a.com.333");
			newNewsItem.setSummary("12345678");
			newNewsItem.setText("Test text 555555555555555555555555555");
			newNewsItem.setRelatedLink("Test text 6666");
			newNewsItem.setRegionId(0);
			newNewsItem.setTopicId(0);
			newNewsItem.setDisplayEmail(true);

			NewsItemAttachment newsItemAttachment = new NewsItemAttachment();
			String fileName = "/Users/zogen/desktop/iphone images/indybay_publisher.png";
			newsItemAttachment.setFilePath(fileName);
			newNewsItem.getAttachments().add(newsItemAttachment);
			
			newsItemAttachment = new NewsItemAttachment();
			fileName = "/Users/zogen/desktop/iphone images/indybay_publisher2.png";
			newsItemAttachment.setFilePath(fileName);
			newNewsItem.getAttachments().add(newsItemAttachment);
			
			newsItemAttachment = new NewsItemAttachment();
			fileName = "/Users/zogen/desktop/iphone images/indybay_publisher3.png";
			newsItemAttachment.setFilePath(fileName);
			newNewsItem.getAttachments().add(newsItemAttachment);
			
			DummyNewsItemPersister persister= new DummyNewsItemPersister();
			persister.save(newNewsItem);
			
			NewsItem newNewsItem2= persister.load();
			
			assertEquals(newNewsItem.getTitle(),newNewsItem2.getTitle());
			assertEquals(newNewsItem.getAuthor(),newNewsItem2.getAuthor());
			assertEquals(newNewsItem.getSummary(),newNewsItem2.getSummary());
			assertEquals(newNewsItem.getText(),newNewsItem2.getText());
			assertEquals(newNewsItem.getAttachments().size(),newNewsItem2.getAttachments().size());

		} catch (final Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * 
	 *//*
	public void testPostWithNoFile() {
		try {
			IndyPreferences preferences= new IndyPreferences();
			final NewsItemPublisher publisher = new NewsItemPublisher();
			final NewsItem newNewsItem = new NewsItem(preferences);
			newNewsItem.setTitle("Test title 111");
			newNewsItem.setAuthor("Test author 222");
			newNewsItem.setEmail("Test email a@a.com.333");
			newNewsItem.setSummary("Test summary 444");
			newNewsItem.setText("Test text 555555555555555555555555555");
			newNewsItem.setRelatedLink("Test text 6666");
			newNewsItem.setRegionId(0);
			newNewsItem.setTopicId(0);
			newNewsItem.setDisplayEmail(true);
			publisher.publishNewsItem(newNewsItem);

		} catch (final Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}*/
}
