package org.indybay.publishhelper.test.implementations;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.indybay.publishhelper.persistence.FileHelper;

/**
 * Main class for web communications
 * 
 * @author Zogren for Indybay.org
 */
public  class DummyFileHelper extends FileHelper{

	/* (non-Javadoc)
	 * @see org.indybay.publishhelper.persistence.FileHelper#getInputStreamFromFilePath(java.lang.String)
	 */
	protected InputStream getInputStreamFromFilePath(String filePath)
			throws IOException {
		File file= new File(filePath);
		return new FileInputStream(file);
	}
	
}
