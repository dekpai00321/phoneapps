/*
 NewsItemAttachment class acting as model class for attachments on NewsItem
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NEWSITEMATTACHMENT_H
#define NEWSITEMATTACHMENT_H

#include <string>
using namespace std;

#include "../common/indypublishbase.h"

//looks like a struct but more may be added later
class NewsItemAttachment : public IndyPublishBase
{
public:
    int attachmentType;
    string filePath;
	void* thumbnail;
};

#endif // NEWSITEMATTACHMENT_H
