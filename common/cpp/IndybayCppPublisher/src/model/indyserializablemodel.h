/*
 IndySerializableModel class for filling in a model from a string
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INDYSERIALIZABLEMODEL_H
#define INDYSERIALIZABLEMODEL_H

#include <string>
#include <vector>
using namespace std;


class IndySerializableModel
{
protected: 	
	bool dirty;	
public:
	void savedSuccessfully();
	bool isDirty();
	void loadFromString(string itemAsString);
	virtual  string toString()=0;
	virtual void  setValueBasedOffKey(string nextKey, string nextValue)=0;
};

#endif // INDYSERIALIZABLEMODEL_H
