/*
 IndyPreferences class for holding default preferences for NewsItem
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "indypreferences.h"
#include "../common/stringhelper.h"

IndyPreferences::IndyPreferences() {
	applicationKey = "at#Hqe_t342qtzz";
	//baseURL = "www.indybay.org";
	baseURL = "69.181.155.87";
	defaultAuthor = "";
	defaultText = "Pictures were posted from my phone using Indybay Publisher.";
	defaultTitle = "Photos From ";
	defaultSummary =""; 
	httpProtocol = "http://";
	relativePublishPageURLPath = "/mobile/mpublish.php";
	resetAuthor = false;
	resetSummary = true;
	resetText = true;
	resetTitle = true;
	maxFilesAllowed = 6;

	StringHelper stringHelper;
	personalKey = "PK" + stringHelper.intToString(time(NULL));
}

string IndyPreferences::toString() {
	StringHelper strhelper;
	string buffer = "";
	buffer.append("baseurl:");
	buffer.append(baseURL);
	buffer.append("\n");
	
	buffer.append("relativepath:");
	buffer.append(relativePublishPageURLPath);
	buffer.append("\n");
	
	buffer.append("resizeamount:");
	buffer.append(strhelper.intToString(resizeAmount));
	buffer.append("\n");
	
	buffer.append("maxattachments:");
	buffer.append(strhelper.intToString(maxFilesAllowed));
	buffer.append("\n");

	buffer.append("resettitle:");
	if (resetTitle) {
		buffer.append("1");
	} else {
		buffer.append("0");
	}
	buffer.append("\n");
	
	buffer.append("defaulttitle:");
	buffer.append(defaultTitle);
	buffer.append("\n");
	
	buffer.append("resetauthor:");
	if (resetAuthor) {
		buffer.append("1");
	} else {
		buffer.append("0");
	}
	buffer.append("\n");
	
	buffer.append("defaultauthor:");
	buffer.append(defaultAuthor);
	buffer.append("\n");
	
	buffer.append("resetsummary:");
	if (resetSummary) {
		buffer.append("1");
	} else {
		buffer.append("0");
	}
	buffer.append("\n");
	
	buffer.append("defaultsummary:");
	buffer.append(strhelper.replaceAll(defaultSummary, '\n', '\t'));
	buffer.append("\n");
	
	buffer.append("resettext:");
	if (resetText) {
		buffer.append("1");
	} else {
		buffer.append("0");
	}
	buffer.append("\n");
	
	buffer.append("defaulttext:");
	buffer.append(strhelper.replaceAll(defaultText, '\n', '\t'));
	buffer.append("\n");
	
	return buffer;
}

void IndyPreferences::setValueBasedOffKey(string nextKey, string nextValue) {
	StringHelper stringHelper;
	if (nextKey != ("")) {
		if (nextKey == "resettitle") {
			if (nextValue == "1") {
				resetTitle = true;
			} else {
				resetTitle = false;
			}
			
			return;
		}
		if (nextKey == "defaulttitle"){
			defaultTitle=nextValue;
		}
		
		if (nextKey == "resetauthor") {
			if (nextValue == "1") {
				resetAuthor = true;
			} else {
				resetAuthor = false;
			}
			
			return;
		}
		if (nextKey == "defaultauthor"){
			defaultAuthor=nextValue;
		}
		
		if (nextKey == "resetsummary") {
			if (nextValue == "1") {
				resetSummary = true;
			} else {
				resetSummary = false;
			}
			
			return;
		}
		if (nextKey == "defaultsummary"){
			defaultSummary=stringHelper.replaceAll(nextValue, '\t', '\n');
		}
		
		if (nextKey == "resettext") {
			if (nextValue == "1") {
				resetText = true;
			} else {
				resetText = false;
			}
			
			return;
		}
		if (nextKey == "defaulttext"){
			defaultText=nextValue;
		}

		
		if (nextKey == "baseurl" && nextValue!="") {
			baseURL = stringHelper.replaceAll(nextValue, '\t', '\n');;
			return;
		}
		if (nextKey == "relativepath" && nextValue!="") {
			
			relativePublishPageURLPath = nextValue;
			return;
		}
		if (nextKey == "resizeamount" && nextValue!="") {
			resizeAmount = stringHelper.stringToInt(nextValue);
			return;
		}
		if (nextKey == "maxattachments" && nextValue !="") {
			maxFilesAllowed = stringHelper.stringToInt(nextValue);
			return;
		}

	}

}

string IndyPreferences::getApplicationKey() const {
	return applicationKey;
}

string IndyPreferences::getBaseURL() const {
	return baseURL;
}

string IndyPreferences::getDefaultAuthor() const {
	return defaultAuthor;
}

string IndyPreferences::getDefaultSummary() const {
	return defaultSummary;
}

string IndyPreferences::getDefaultText() const {
	return defaultText;
}

string IndyPreferences::getDefaultTitle() const {
	return defaultTitle;
}

string IndyPreferences::getHttpProtocol() const {
	return httpProtocol;
}

int IndyPreferences::getMaxFilesAllowed() {
	return maxFilesAllowed;
}

string IndyPreferences::getPersonalKey() const {
	return personalKey;
}

string IndyPreferences::getRelativePublishPageURLPath() const {
	return relativePublishPageURLPath;
}

bool IndyPreferences::getResetAuthor() {
	return resetAuthor;
}

bool IndyPreferences::getResetSummary() {
	return resetSummary;
}

bool IndyPreferences::getResetText() {
	return resetText;
}

bool IndyPreferences::getResetTitle() {
	return resetTitle;
}

int IndyPreferences::getResizeAmount() {
	return resizeAmount;
}

void IndyPreferences::setBaseURL(string baseURL) {
	if (this->baseURL != baseURL) {
		this->baseURL = baseURL;
		this->dirty = true;
	}
}


void IndyPreferences::setDefaultAuthor(string defaultAuthor) {
	if (this->defaultAuthor != defaultAuthor) {
		this->defaultAuthor = defaultAuthor;
		this->dirty = true;
	}
}

void IndyPreferences::setDefaultSummary(string defaultSummary) {
	if (this->defaultSummary != defaultSummary) {
		this->defaultSummary = defaultSummary;
		this->dirty = true;
	}
}

void IndyPreferences::setDefaultText(string defaultText) {
	if (this->defaultText != defaultText) {
		this->defaultText = defaultText;
		this->dirty = true;
	}
}

void IndyPreferences::setDefaultTitle(string defaultTitle) {
	if (this->defaultTitle != defaultTitle) {
		this->defaultTitle = defaultTitle;
		this->dirty = true;
	}
}

void IndyPreferences::setMaxFilesAllowed(int maxFilesAllowed) {
	if (this->maxFilesAllowed != maxFilesAllowed) {
		this->maxFilesAllowed = maxFilesAllowed;
		this->dirty = true;
	}
}

void IndyPreferences::setPersonalKey(string personalKey) {
if (this->personalKey != personalKey) {
	this->personalKey = personalKey;
	this->dirty = true;
}
}

void IndyPreferences::setRelativePublishPageURLPath(
	string relativePublishPageURLPath) {
if (this->relativePublishPageURLPath != relativePublishPageURLPath) {
	this->relativePublishPageURLPath = relativePublishPageURLPath;
	this->dirty = true;
}
}

void IndyPreferences::setResetAuthor(bool resetAuthor) {
if (this->resetAuthor != resetAuthor) {
	this->resetAuthor = resetAuthor;
	this->dirty = true;
}
}

void IndyPreferences::setResetSummary(bool resetSummary) {
if (this->resetSummary != resetSummary) {
	this->resetSummary = resetSummary;
	this->dirty = true;
}
}

void IndyPreferences::setResetText(bool resetText) {
if (this->resetText != resetText) {
	this->resetText = resetText;
	this->dirty = true;
}
}

void IndyPreferences::setResetTitle(bool resetTitle) {
if (this->resetTitle != resetTitle) {
	this->resetTitle = resetTitle;
	this->dirty = true;
}
}

void IndyPreferences::setResizeAmount(int resizeAmount) {
if (this->resizeAmount != resizeAmount) {
	this->resizeAmount = resizeAmount;
	this->dirty = true;
}
}

