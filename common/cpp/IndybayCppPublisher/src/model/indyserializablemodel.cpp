/*
 IndySerializableModel class for filling in a model from a string
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "indyserializablemodel.h"
#include "../common/stringhelper.h"

bool IndySerializableModel::isDirty(){
	return dirty;
}
void IndySerializableModel::savedSuccessfully(){
	dirty=false;
}

void IndySerializableModel::loadFromString(string itemAsString) {
	StringHelper stringHelper;
    int i1 = 0;
    int i2 = 0;
    string nextValue="";
    string nextKey="";
    string newLineChar="\n";
	string* keyValue=NULL;
    itemAsString = newLineChar + itemAsString;
    while (i1 > -1) {
		nextKey="";
		nextValue="";
        i1 = itemAsString.find(newLineChar, i1);
		if (i1>=0){
			i1++;
			i2=  itemAsString.find(newLineChar, i1 + 1);
			if (i2<0){
				i2=itemAsString.length();
			}
			keyValue=stringHelper.getKeyAndValueFromString(itemAsString,i1,i2);
			if (keyValue!=NULL){
				nextKey=keyValue[0];
				nextValue=keyValue[1];
			}
		}
        
		if (nextKey!=("")) {
			this->setValueBasedOffKey(nextKey,nextValue);
        }
    }
	dirty=false;

}

