/*
 StringHelper class for providing common string related functionality needed by IndybayPublisher code
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "stringhelper.h"

#include <sstream>
#include <string>
#include <math.h>

//converts and int to a string
string StringHelper::intToString(int i){
    stringstream sstr;
    sstr << i;
    string newStr = sstr.str();
    return newStr;
}

//replace up to 1000 occurances of str2 in str1 with str3
string StringHelper::replaceAll(string str1, char char1, char char2){
	string ret;
	if (str1.size()==0)
		ret=str1;
	else{
	char* newCharArray= new char[str1.length()];
	strcpy(newCharArray,str1.c_str());
	
	for (int i=0;i<str1.length();i++){
		if (newCharArray[i]==char1){
			newCharArray[i]=char2;
		}
	}
	ret=newCharArray;
	}
	
	return ret;
	
}

//converts a string to an int
int StringHelper::stringToInt(string str){
    return atoi(str.c_str());
}

//converts a string to an int
string StringHelper::intToHexString(int i){
	char buffer[20];
	int n=sprintf(buffer,"%x",i);
	buffer[n+1]='\0';
	string newString=buffer;
    return newString;
}

int StringHelper::hexDigitToInt(char digit){
	int ret=0;
	if (digit>='0' && digit<='9'){
		ret=digit-'0';
	}else if (digit>='a' && digit<='f'){
		ret=digit-'a'+10;
	}else if (digit>='A' && digit<='F'){
		ret=digit-'A'+10;
	} 
	return ret;
}

int StringHelper::hexStringToInt(string str){
	int ret=0;
	const char* nextchar=str.c_str();;
	int digit=0;
	for (int i=str.size()-1;i>-1;i--){
		if (nextchar[i]<'!')
			continue;
		ret=ret+(int)hexDigitToInt(nextchar[i])*(int)pow(16,digit);
		digit++;
	}
	return ret;
}

string* StringHelper::getKeyAndValueFromString(string str, int startLineIndex, int endLineIndex){
	int i=str.find(":",startLineIndex);
	string* returnString=NULL;
	if (i<endLineIndex+1){
		returnString= new string[2];
		returnString[0]=str.substr(startLineIndex,i-startLineIndex);
		returnString[1]=str.substr(i+1, endLineIndex-(i+1));
	}
	return returnString;
	
}