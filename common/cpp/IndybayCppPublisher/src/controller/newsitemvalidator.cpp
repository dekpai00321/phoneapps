/*
 NewsItemValidator class for validating news item for IndybayPublisher code
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "newsitemvalidator.h"

vector<string>
NewsItemValidator::validateNewsItem(NewsItem* newsItem)
{

  string errorMessage;
  vector<string> validationProblems;

  if (newsItem->getTitle() == "")
    {
      validationProblems.push_back("Title Is Required");
    }
  else if (newsItem->getTitle().size() < 8)
    {
      validationProblems .push_back(
          "Title Must Be At Least 8 Characters In Length");
    }
  if (newsItem->getAuthor() == "")
    {
      validationProblems.push_back("Author Is Required");
    }
  if (newsItem->getSummary() == "")
    {
      validationProblems.push_back("Summary Is Required");
    }
  else if (newsItem->getSummary().size() < 8)
    {
      validationProblems .push_back(
          "Summary Must Be At Least 8 Characters In Length");
    }
  if ((newsItem->getAttachments().size() == 0) && (newsItem->getSummary() == ""))
    {
      validationProblems.push_back("Text Is Required");
    }
  else if ((newsItem->getAttachments().size() == 0) && (newsItem->getText().size() < 12))
    {
      validationProblems .push_back(
          "Text Must Be At Least 12 Characters In Length");
    }
  return validationProblems;
}
