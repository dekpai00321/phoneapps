#ifndef NEWSITEMPUBLISHER_H
#define NEWSITEMPUBLISHER_H

#include <string>
using namespace std;

#include "../model/newsitem.h"
#include "../net/webhelper.h"
#include "../model/indypreferences.h"
#include "../model/indyserializablemodel.h"

class NewsItemPublisher : public IndyPublishBase
{

private:
    string lastError;
	//Preferences object containing base url and what to do after publish occurs
	IndyPreferences* preferences;
public:
    NewsItemPublisher(IndyPreferences* newPreferences);
    vector<string> publishNewsItem(NewsItem* newsItem);
    int getNewsItemIdFromURL(string resultURL);
    string getURLFromResultString(string resultString);
    map<string,string> prepareFields(NewsItem* newsItem);
	int getRecentNetworkSpeedUploading();
	int getRecentNetworkSpeedDownloading();
};

#endif // NEWSITEMPUBLISHER_H
