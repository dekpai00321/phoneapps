#include "newsitempublisher.h"
#include "newsitemvalidator.h"
#include "../common/stringhelper.h"


/**
 * Constructor requires a reference to a preference object that keeps tract
 * of where to post, what to do after posting etc...
 */
NewsItemPublisher::NewsItemPublisher(IndyPreferences* newPreferences) {
	lastError="";
    preferences = newPreferences;
}


//Main publish action that POSTs to Indybay
//uses a pointer since news item is modified after post
vector<string> NewsItemPublisher::publishNewsItem( NewsItem* newsItem) {
	WebHelper webHelper;
    NewsItemValidator validator;
	
    vector<string> validationProblems = validator.validateNewsItem(newsItem);
    if (validationProblems.size() > 0) {
        return validationProblems;
    }
	
    map<string,string> postFields = prepareFields(newsItem);
	
    string results = webHelper.httpMIMEPOST(preferences->getBaseURL(),preferences->getRelativePublishPageURLPath(), postFields, newsItem->getAttachments());
    string resultURL = getURLFromResultString(results);
	if (resultURL==""){
		if (lastError!=""){
			validationProblems.push_back(lastError);
			lastError="";
		}else {
			validationProblems.push_back(lastError);
		}
		
	}else{
		int newsItemId = getNewsItemIdFromURL(resultURL);
		if (newsItemId!=-1){
			newsItem->setId(newsItemId);
		}else if (lastError!=""){
			validationProblems.push_back(lastError);
			lastError="";
		}
		newsItem->setPostedURL(resultURL);
		newsItem->setPublishDate(time(NULL));
		newsItem->removeAllAttachments();
    }
	return validationProblems;
}

/**
 * Given a URL of a result after POSTing, this method returns the id portion
 * of the url
 *
 * @param resultURL
 * @return
 * @throws ServerSidePublishException
 */
int NewsItemPublisher::getNewsItemIdFromURL( string resultURL)
{
    int i = -1;
    int ii = resultURL.find("/");
    while (ii > -1) {
        i = ii;
        ii = resultURL.find("/", ii + 1);
    }
    if (i < 0) {
        lastError="Returned URL was in an unexpected format";
		return -1;
    }
    int j = resultURL.find(".php", i);
    if ((j < 0) || (j - i > 100)) {
		lastError="Returned URL was not for a php page";
		return -1;
    }
    int k = 0;
    try {
        StringHelper* stringHelper= new StringHelper();
        k=stringHelper->stringToInt(resultURL.substr(i + 1, j));
		
    } catch ( ...) {
		lastError= "ID in returned URL was non-numeric: "
		+ resultURL.substr(i + 1, j);
		return -1;
    }
    return k;
}

/**
 * Parses a result page and returns the url of the just posted item
 *
 * @param resultstring
 * @return url
 * @throws ServerSidePublishException
 */
string NewsItemPublisher::getURLFromResultString( string resultString)
{
    int i = resultString.find("SUCCESS");
    if (i < 0) {
		lastError= "Publish failed to return result success page";
		return "";
    }
    i = resultString.find("/newsitems/", i);
    if (i < 0) {
		lastError= "Publish failed to return link to resulting post";
		return "";
    }
    int j = resultString.find(".php", i);
    if (j < 0) {
		
		lastError= "Publish failed to well formatted link to resulting post";
		return "";
    }
    string url = resultString.substr(i, j + 4);
    url = "http://" + preferences->getBaseURL() + url;
	
    return url;
}

/**
 * Sets up a HashMap with name value pairs for the nonfile data to be posted
 *
 * @param newsItem
 * @return hashMap
 * @throws ConnectionException
 * @throws ConfigurationException
 */
map<string,string> NewsItemPublisher::prepareFields( NewsItem* newsItem) {
    StringHelper stringHelper;
    map<string,string> fieldMap;
	
    string titleKey="title";
    string authorKey="author";
    string summaryKey="summary";
    string textKey="text";
    string relatedURLKey="related_url";
    string emailKey="email";
    string displayEmailKey="display_contact_info";
    string regionIdKey="region_id";
    string topicIdKey="topic_id";
    string displayEmailTrue="1";
    string publishKey="publish";
    string publishValue="PUBLISH";
    string displayEmailFalse="";
    string applicationKey="application_key";
    string userKey="user_key";
    string fileCountKey="file_count";
	
    fieldMap.insert(
					pair<string,string>(titleKey,newsItem->getTitle()));
    fieldMap.insert( pair<string,string>(authorKey, newsItem->getAuthor()));
    fieldMap.insert( pair<string,string>(summaryKey, newsItem->getSummary()));
    fieldMap.insert( pair<string,string>(textKey, newsItem->getText()));
    fieldMap.insert( pair<string,string>(relatedURLKey, newsItem->getRelatedLink()));
    fieldMap.insert( pair<string,string>(emailKey, newsItem->getEmail()));
	
    if (newsItem->getDisplayEmail()) {
        fieldMap.insert(pair<string,string>(displayEmailKey, displayEmailTrue));
    } else {
        fieldMap.insert(pair<string,string>(displayEmailKey, displayEmailFalse));
    }
	
    fieldMap.insert(pair<string,string>(regionIdKey, stringHelper.intToString(newsItem->getRegionId())));
	
    fieldMap.insert(pair<string,string>(topicIdKey,  stringHelper.intToString(newsItem->getTopicId())));
	
    fieldMap.insert(pair<string,string>(publishKey, publishValue));
	
    fieldMap.insert(pair<string,string>(applicationKey, preferences->getApplicationKey()));
    fieldMap.insert(pair<string,string>(userKey, preferences->getPersonalKey()));
	
    fieldMap.insert(pair<string,string>(fileCountKey, stringHelper.intToString(newsItem->getAttachments().size())));
    return fieldMap;
}

int NewsItemPublisher::getRecentNetworkSpeedUploading(){
	return 0;
}


int NewsItemPublisher::getRecentNetworkSpeedDownloading(){
	return 0;
}



