/*
 FileHelper class for handling common File IO issues
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <dirent.h>
#include <iostream>

#include "filehelper.h"
#include "../common/stringhelper.h"

//generates a unique file name for a file extension
string FileHelper::generateUniqueFileName( string extension){
    StringHelper stringHelper;
    string fileName = stringHelper.intToString((int)time(NULL));
    if (fileName.length() > 5) {
        fileName = fileName.substr(fileName.length() - 5, fileName
                                      .length());
    }
    fileName = "p" + fileName + extension;

    return fileName;
}

//return size in bits of a file
int FileHelper::getFileSize(const char* filePath){
	FILE* fp;
	int fileSize=0;
	string chunkSizeStr;
    if ((fp=fopen(filePath,"r"))==NULL){
		string errorMessage;
		errorMessage.append("Cannot open file ");
		errorMessage+=filePath;
		logger.logError(errorMessage);
		return -1;
    }else{
		fseek(fp, 0L, SEEK_END);
		fileSize = ftell(fp);
		fseek(fp, 0L, SEEK_SET);
		return fileSize;
	}
		
		
}
			

//returns the file name portion of a file path
string FileHelper::getFilenameFromFilePath(string filePath){
    int j = filePath.find("/");
    int i = 0;
    while (j > -1) {
        i = j;
        j = filePath.find("/", i + 1);
    }
    string fileName;
    if (i > 0) {
        fileName = filePath.substr(i + 1);
    } else if (i == 0) {
        fileName = filePath;
    }
    return fileName;
}

//loads contents of a file into a string (checking header
//and loading with chunk encoding or by content length)
string FileHelper::getStringFromFile(char* filePath){
    FILE* fp;
    string str;
    char* data;
    int numberOfBytesActuallyRead;

    if ((fp=fopen(filePath,"r"))==NULL){
        logger.logError("Cannot open file");
		return "";
    }else{
        data=(char*)calloc(1000,sizeof(char));
        while (!feof(fp)){
            numberOfBytesActuallyRead=fread(data,1,1000,fp);
            str.append(data,numberOfBytesActuallyRead);
        }
        free(data);
    }
    return str;

}

//writes a string to a file
bool FileHelper::writeStringToFile(string str, char* filePath){
    FILE* fp;
    if ((fp=fopen(filePath,"w"))==NULL){
		string err="";
        err+="Cannot open file ";
		err+=filePath;
		logger.logError(str);
		return false;
    }else{
        fwrite(str.c_str(),sizeof(char),str.length(),fp);
    }
	return true;
}

vector<string> FileHelper::listFilesInDirectoryByExtension(string directoryPath, string extension){
		DIR *dp;
		dirent *dirp;
		vector<string> files;
		if((dp  = opendir(directoryPath.c_str())) == NULL) {
			logger.logError("Cannot open directory");
			return files;
		}
		
		while ((dirp = readdir(dp)) != NULL) {
			if (strstr(dirp->d_name, extension.c_str())>0){
				files.push_back(string(directoryPath+"/"+dirp->d_name));
			}
		}
		closedir(dp);
		return files;
	}

bool FileHelper::deleteFilesInDirectoryByExtension(string directoryPath, string extension){
	vector<string> filesToDelete;
	filesToDelete=listFilesInDirectoryByExtension(directoryPath, extension);
	int success;
	string nextPathToDelete;
	for (int i=0;i<filesToDelete.size();i++){
		success=success&&remove(filesToDelete.at(i).c_str());
	}
	if (!success){
		logger.logError("Could Not Delete File");
		return false;
	}
	return true;
}

