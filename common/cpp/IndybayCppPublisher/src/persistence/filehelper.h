/*
 FileHelper class for handling common File IO issues
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILEHELPER_H
#define FILEHELPER_H

#include <vector>
#include <string>
#include <fstream>
using namespace std;

#include "../common/iohelper.h"

class FileHelper: public IOHelper {

public:

	int getFileSize(const char* filePath);

	string generateUniqueFileName(string extension);

	string getFilenameFromFilePath(string filePath);

	string getStringFromFile(char* filePath);

	bool writeStringToFile(string str, char* filePath);

	vector<string> listFilesInDirectoryByExtension(string directoryPath,
			string extension);
	bool deleteFilesInDirectoryByExtension(string directoryPath,
			string extension);
};

#endif // FILEHELPER_H
