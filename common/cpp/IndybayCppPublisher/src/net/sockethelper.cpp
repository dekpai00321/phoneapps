/*
 SocketHelper class which is parent class of socket readig and writing classes
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#import "sockethelper.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "../common/stringhelper.h"
#include <math.h>

SocketHelper::SocketHelper():IOHelper(){
}



int SocketHelper::setupSocketAndConnect(string baseURL){
	struct addrinfo hints, *res;
    int sockfd;
	
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(baseURL.c_str(), "http", &hints, &res);
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	
    connect(sockfd, res->ai_addr, res->ai_addrlen);
	return sockfd;
}
