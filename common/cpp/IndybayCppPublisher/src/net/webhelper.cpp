/*
 WebHelper class for posting and receiving via HTTP (low level functionality are in socket classes used by this)
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "webhelper.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "../common/stringhelper.h"
#include <math.h>
#include "webhelper.h"

int WebHelper::bytesSentSoFarForCurrentPost=0;
int WebHelper::estimatedBytesToSendTotalForCurrentPost=0;
time_t WebHelper::lastPOSTStartTime=-1;

WebHelper::WebHelper():IOHelper(){
	mimeBoundary="0194784892923";
	mimeDivider = "--" + mimeBoundary+ clrf;
}


bool WebHelper::httpGETHelper(int sockfd, string getStringToSend){
	if (!sender.sendStringOverSocket(sockfd,getStringToSend,false))
		return false;
	lastHTTPResponse=reader.getFromSocket(sockfd);
	if (lastHTTPResponse=="" && IndyPublishLogger::lastErrorMessage!="")
		return false;
	return true;
}


bool WebHelper::httpPOSTHelper(int sockfd, string postHeader, map<string,string> postFields, vector<NewsItemAttachment> attachments){
	bytesSentSoFarForCurrentPost=0;
	lastPOSTStartTime=time(NULL);
	logger.logInfo("STARTED SENDING...");

	sender.sendStringOverSocket(sockfd,postHeader,false);
	
	string postFieldsString=getStringForPostFields(postFields);
	
	if (!sender.sendStringOverSocket(sockfd,clrf+postFieldsString,true))
		return false;
	
	if (attachments.size()>0) {
		if (!writeOutAttachments(sockfd,attachments))
			return false;
	}
	if (!sender.sendStringOverSocket(sockfd,"--" + mimeBoundary+ "--",true))
		return false;
	
	string postFieldsFooter=clrf+"0"+clrf+clrf;
	if (!sender.sendStringOverSocket(sockfd,postFieldsFooter,false))
		return false;
	logger.logInfo("DONE SENDING...");
	logger.logInfo("GETTING...");
	lastHTTPResponse=reader.getFromSocket(sockfd);
	if (lastHTTPResponse=="" && IndyPublishLogger::lastErrorMessage!="")
		return false;
	logger.logInfo("DONE GETTING...");
	return true;
}

void* httpGETThreadedHelper(void* pv){
	printf("httpGETThreadedHelper\n");
	IndyWebHelperThreadArgs* args = static_cast<IndyWebHelperThreadArgs*>(pv) ;
    WebHelper* This = args->This ;
	
	This->isHTTPThreadStillRunning=true;
	try {
		This->httpGETHelper(args->sockfd,args->headerString);
		This->isHTTPThreadStillRunning=false;
	}
	catch (IndyPublishException* e) {
		This->isHTTPThreadStillRunning=false;
		This->lastThreadError=e;
		
	}
	pthread_exit(pv);
	
}

void* httpPOSTThreadedHelper(void* pv){
	IndyWebHelperThreadArgs* args = static_cast<IndyWebHelperThreadArgs*>(pv) ;
    WebHelper* This = args->This ;
	
	This->isHTTPThreadStillRunning=true;
	try {
		This->httpPOSTHelper(args->sockfd,args->headerString, args->postFields, args->attachments);
		This->isHTTPThreadStillRunning=false;
	}
	catch (IndyPublishException* e) {
		This->isHTTPThreadStillRunning=false;
		This->lastThreadError=e;
		
	}
	pthread_exit(pv);
}



string WebHelper::httpGET(string baseURL, string additionalPath){
    int sockfd=reader.setupSocketAndConnect(baseURL);
		logger.logInfo("GETTING...");
		string getLine="GET "+additionalPath+" HTTP/1.1"+clrf +
		"Host: "+baseURL+clrf+clrf;
		
		IndyWebHelperThreadArgs* args=new IndyWebHelperThreadArgs(this,sockfd, getLine);
		if (!runThreadAndWaitForResponse(httpGETThreadedHelper,args,30))
			return "";
		close (sockfd);
		return this->lastHTTPResponse;
}

string WebHelper::httpMIMEPOST(string baseURL, string additionalPath, map<string,string> postFields,
							   vector<NewsItemAttachment> attachments){
	int sockfd=reader.setupSocketAndConnect(baseURL);
		string postHeader="POST "+additionalPath+" HTTP/1.1"+clrf +
		"Host: "+baseURL+clrf +
		"Transfer-Encoding: chunked"+clrf +
		"User-Agent: IndybayCppPublisher"+clrf+
		"Content-Type: multipart/form-data; boundary="+mimeBoundary+clrf;
				
		IndyWebHelperThreadArgs* args=new IndyWebHelperThreadArgs(this,sockfd, postHeader,postFields, attachments);
	runThreadAndWaitForResponse(httpPOSTThreadedHelper,args,45+60*attachments.size());
		delete(args);

		close (sockfd);
		string ret=this->lastHTTPResponse;
		logger.logDebug("leaving httpMIMEPOST");
		return ret;
	
}


bool  WebHelper::writeOutAttachments(int sockfd, vector<NewsItemAttachment> attachments){
	NewsItemAttachment nextAttachment;
	unsigned int j = 0;
	FileHelper fileHelper;
	string postContent;
	StringHelper stringHelper;
	int totalAttachmentSize=0;
	for (j = 0; j < attachments.size(); j++) {
		nextAttachment =  attachments.at(j);
		totalAttachmentSize+=fileHelper.getFileSize(nextAttachment.filePath.c_str());
	}
	//attachment size plus header stuff
	estimatedBytesToSendTotalForCurrentPost=totalAttachmentSize+10000;
	for (j = 0; j < attachments.size(); j++) {
		postContent = "";
		nextAttachment =  attachments.at(j);
		postContent.append(mimeDivider);
		postContent.append("Content-Disposition: form-data; name=\"");
		postContent.append("linked_file_");
		postContent.append(
							stringHelper.intToString(j+1));
		postContent.append("\"; filename=\"");
		
		postContent.append(fileHelper
							.getFilenameFromFilePath(nextAttachment.filePath));
		postContent.append("\""+clrf);
		postContent.append("Content-Type: application/octet-stream"+clrf+clrf);
		
		if (!sender.sendStringOverSocket(sockfd,postContent,true))
			return false;

		if (!sender.copyFileDataToSocket(sockfd,nextAttachment.filePath.c_str(),true))
			return false;
		
		if (!sender.sendStringOverSocket(sockfd,clrf,true))
			return false;

	}
	return true;
}


string WebHelper::getStringForPostFields(map<string,string> postFields) {
	string postContent = "";
	map<string,string>::iterator iter;
	string nextKey;
	string nextValue;
	for ( iter=postFields.begin() ; iter != postFields.end(); iter++ ){
		nextKey = (*iter).first;
		nextValue = (*iter).second;
		postContent.append(mimeDivider);
		postContent.append("Content-Disposition: form-data; name=\"");
		
		postContent.append(nextKey);
		postContent.append("\""+clrf+clrf);
		postContent.append(nextValue);
		postContent.append(clrf);
	}
	return postContent;
}


//setup as seperate function to allow 
bool WebHelper::runThreadAndWaitForResponse(void* (*functionToRun)(void*) , void* args, int timeoutInSeconds){
	pthread_t receivingThread;
	
	isHTTPThreadStillRunning=true;
	int rc=pthread_create(&receivingThread, NULL, functionToRun, args);
	if (rc){
		StringHelper stringHelper;
		string err="Return code from pthread_create() was "+stringHelper.intToString(rc);
		logger.logError(err);
		return false;
	}
	int i=0;
	int pollingInternal=10000;
	while(isHTTPThreadStillRunning){
		if (i>=(timeoutInSeconds*1000000)/pollingInternal){
			pthread_kill(receivingThread, 9);
			logger.logError("Timeout!!");
			return false;
		}
		usleep(pollingInternal);
		i++;
	}	
	return true;
	
}





