/*
 WebHelper class for posting and receiving via HTTP (low level functionality are in socket classes used by this)
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WEBHELPER_H
#define WEBHELPER_H

#include <map>
#include <vector>
#include <string>
#import <fstream>
using namespace std;

#import "../common/iohelper.h"
#import "../persistence/filehelper.h"
#import "../model/newsitemattachment.h"
#import "httpsocketreader.h"
#import "httpsocketwriter.h"
#include "../common/indypublishexception.h";



class WebHelper : protected IOHelper
{
private:
	HTTPSocketReader reader;

	
	
	
	string mimeBoundary, mimeDivider;
	string getStringForPostFields(map<string,string> postFields);
	bool writeOutAttachments(int sockfd, vector<NewsItemAttachment> attachments);
	bool runThreadAndWaitForResponse(void* (*functionToRun)(void*) , void* args, int timeoutInMilliseconds);

public:
	
	static int bytesSentSoFarForCurrentPost;
	static int estimatedBytesToSendTotalForCurrentPost;
	static time_t lastPOSTStartTime;
	
	//called by the threads
	string lastHTTPResponse;
	bool isHTTPThreadStillRunning;
	IndyPublishException* lastThreadError;
	bool httpGETHelper(int sockfd, string getStringToSend);
	bool httpPOSTHelper(int sockfd, string postHeader, map<string,string> postFields, vector<NewsItemAttachment> attachments);
	
	HTTPSocketWriter sender;
	
	WebHelper();
    string httpGET( string baseURL, string path);
    string httpMIMEPOST(string baseURL, string path, map<string,string> postFields,
                     vector<NewsItemAttachment> attachments);

};


struct IndyWebHelperThreadArgs
{
	WebHelper* This ;
	int sockfd;
	string headerString;
	map<string,string> postFields;
	vector<NewsItemAttachment> attachments;
	IndyWebHelperThreadArgs( WebHelper* t, int s, string h) 
	: This(t), sockfd(s), headerString(h) {}
	IndyWebHelperThreadArgs( WebHelper* t, int s, string h, map<string,string> p, vector<NewsItemAttachment> a) 
	: This(t), sockfd(s), headerString(h), postFields(p), attachments(a) {}
};





#endif // WEBHELPER_H
