/*
 HTTPSocketWriter class for helping send chunked data over HTTP 
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HTTPSOCKETWRITER_H
#define HTTPSOCKETWRITER_H

#include <map>
#include <vector>
#include <string>
#import <fstream>
using namespace std;

#import "sockethelper.h"
#import "../persistence/filehelper.h"
#import "../model/newsitemattachment.h"

class HTTPSocketWriter : public SocketHelper {
private:
	int sendBytesOverSocketHelper(int sockfd, const void* bytes, int amountToSend);	
	bool sendChunkSize(int sockfd, int chunkSize);
public:
	bool sendBytesOverSocket(int sockfd, const void* bytes, int amountToSend, bool sendChunkSizeFirst);
	bool sendStringOverSocket(int sockfd, string str, bool sendChunkSizeFirst);
	bool copyFileDataToSocket(int sockfd, const char* filePath, bool addChunkSizes);
};




#endif // HTTPSOCKETWRITER_H
