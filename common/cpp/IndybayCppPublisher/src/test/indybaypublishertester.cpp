/*
 * IndybayPublisherTester.cpp
 *
 *  Created on: Mar 11, 2010
 *      Author: zogen
 */

#include "indybaypublishertester.h"
#include "../net/webhelper.h"
#include "../model/newsitemattachment.h"
#include "../common/indypublishexception.h"
#include "../controller/newsitempublisher.h"

IndybayPublisherTester::IndybayPublisherTester() {
}


void IndybayPublisherTester::runGETTest() {
	IndyPublishLogger logger;
	WebHelper webHelper;
	string str = "127.0.0.1";
	string res;

	logger.logInfo("TEST GET");

	try {
		res = webHelper.httpGET(str, "/publish.php");
	} catch (IndyPublishException* e) {
		logger.logError("Unit Test \"runGETTest\" Failed");
		return;
	}
	logger.logInfo("GOT: " + res);

}

NewsItem  IndybayPublisherTester::getTestNewsItem(bool withAttachments, bool hasValidatorErrors) {
	IndyPreferences prefs;
	NewsItem newsItem= *(new NewsItem(&prefs));
	NewsItemAttachment attachment1;
	NewsItemAttachment attachment2;
	if (hasValidatorErrors)
	       newsItem.setTitle("Test");
	else
		newsItem.setTitle("Test Title 12345");
	newsItem.setAuthor("Test Author 2");
	newsItem.setSummary("Test Summary 3 adf fasd fasdf asdf ");
	newsItem.setText("Test Text 4 fasd fasd fasdfasd fads fasd fasd fasdfasdf ");
	if (withAttachments) {
		attachment1.filePath
				= "/projects/indybay-active/website/images/antiglob.jpg";
		newsItem.addAttachment(attachment1);
		attachment2.filePath
				= "/projects/indybay-active/website/images/banner_art.jpg";
		newsItem.addAttachment(attachment2);
	}
	return newsItem;
}

void IndybayPublisherTester::runPOSTTest(bool includeFiles) {
	IndyPublishLogger logger;

	WebHelper webHelper;
	IndyPreferences preferences;
	map<string, string> postFields;
	NewsItem newsItem = getTestNewsItem(includeFiles, true);
	string str = "127.0.0.1";

	NewsItemPublisher* newsItemPublisher = new NewsItemPublisher(&preferences);

	if (includeFiles)
		logger.logInfo("TEST POST MULTIPLE FILES");
	else
		logger.logInfo("TEST POST NO FILES");

	logger.logDebug("preparing fields");

	try {
		postFields = newsItemPublisher->prepareFields(&newsItem);
	} catch (IndyPublishException* e) {
		logger.logError("Unit Test \"runPOSTTest\" Failed");
		delete (newsItemPublisher);
		return;
	}

	logger.logDebug("posting via MIME");
	string res;
	try {
		res = webHelper.httpMIMEPOST(str, "/mobile/mpublish.php", postFields,
				newsItem.getAttachments());
	} catch (IndyPublishException* e) {
		logger.logInfo("Unit Test \"runPOSTTest\" Failed");
		delete (newsItemPublisher);
		return;
	}
	logger.logInfo("GOT:" + res);
	delete (newsItemPublisher);

}

void IndybayPublisherTester::runNewsItemPublishTest(bool includeFiles, bool hasValidatorErrors) {
	IndyPublishLogger logger;
	IndyPreferences preferences;
	NewsItemPublisher* newsItemPublisher = new NewsItemPublisher(&preferences);
	NewsItem newsItem = getTestNewsItem(includeFiles, hasValidatorErrors);
	vector<string> errors=newsItemPublisher->publishNewsItem(&newsItem);

	if (errors.size()>0){
		logger.logError("More than one error occurred");
		for (unsigned int i=0;i<errors.size();i++){
			string nextError=errors.at(i);
			logger.logError(nextError);
		}
	}
	delete(newsItemPublisher);

}


void IndybayPublisherTester::runTests() {
	runGETTest();
	runPOSTTest(false);
	runPOSTTest(true);
	runNewsItemPublishTest(false, true);
	runNewsItemPublishTest(true,true );
    runNewsItemPublishTest(false, false);
    runNewsItemPublishTest(true,false );
}

