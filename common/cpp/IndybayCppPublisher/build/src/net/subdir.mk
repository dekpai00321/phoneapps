################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/net/httpsocketreader.cpp \
../src/net/httpsocketwriter.cpp \
../src/net/sockethelper.cpp \
../src/net/webhelper.cpp 

OBJS += \
./src/net/httpsocketreader.o \
./src/net/httpsocketwriter.o \
./src/net/sockethelper.o \
./src/net/webhelper.o 

CPP_DEPS += \
./src/net/httpsocketreader.d \
./src/net/httpsocketwriter.d \
./src/net/sockethelper.d \
./src/net/webhelper.d 


# Each subdirectory must supply rules for building sources it contributes
src/net/%.o: ../src/net/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


