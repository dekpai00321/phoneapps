################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/common/indypublishbase.cpp \
../src/common/indypublishexception.cpp \
../src/common/indypublishlogger.cpp \
../src/common/iohelper.cpp \
../src/common/stringhelper.cpp 

OBJS += \
./src/common/indypublishbase.o \
./src/common/indypublishexception.o \
./src/common/indypublishlogger.o \
./src/common/iohelper.o \
./src/common/stringhelper.o 

CPP_DEPS += \
./src/common/indypublishbase.d \
./src/common/indypublishexception.d \
./src/common/indypublishlogger.d \
./src/common/iohelper.d \
./src/common/stringhelper.d 


# Each subdirectory must supply rules for building sources it contributes
src/common/%.o: ../src/common/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


