################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/model/indypreferences.cpp \
../src/model/newsitem.cpp \
../src/model/newsitemattachment.cpp 

OBJS += \
./src/model/indypreferences.o \
./src/model/newsitem.o \
./src/model/newsitemattachment.o 

CPP_DEPS += \
./src/model/indypreferences.d \
./src/model/newsitem.d \
./src/model/newsitemattachment.d 


# Each subdirectory must supply rules for building sources it contributes
src/model/%.o: ../src/model/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


