/*
 PhotoPageViewController class is view controller for the Attachment List page
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>

#import "IndyNewsViewController.h"


@interface PhotoPageViewController : IndyNewsViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>  {
	IBOutlet UITableView *imageTableView;
	IBOutlet UIBarButtonItem *editButton;
	IBOutlet UIBarButtonItem *addButton;
	IBOutlet UISlider *resizeSlider;
	IBOutlet UILabel *maxNumberAttachmentsLabel;
	IBOutlet UINavigationBar* navBar;
	UIImagePickerController* imagePickerView;
	
}
- (IBAction)selectPhoto:(id)sender;
- (IBAction)setTableToEditMode:(id)sender;
- (IBAction)setTableToNotEditMode:(id)sender;
- (IBAction)resizeSliderChanged:(id)sender;

- (void)selectPhotoHelper:(int)source;
- (void)displayImage:(UIImage*)image;


@end
