/*
 IndyNewsPublisherAppDelegate is the main controller class for the app
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>

#import "../../IndybayCppPublisher/src/model/newsitem.h"
#import "Reachability.h"



@interface IndyNewsPublisherAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
@public	
	UIWindow *window;
    UITabBarController *tabBarController;
	NewsItem* currentItemToPublish;
	IndyPreferences* preferences;
	NetworkStatus serverReachable;
	Reachability* serverReachability;
}


@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

- (IBAction)indymediaIconClick:(id)sender;
- (NSString *)applicationDocumentsDirectory;
- (void) reachabilityChanged: (NSNotification* )note;
-(void)saveCurrentNewsItem;
-(void)savePreferences;
- (NewsItem*) newsItem;
- (IndyPreferences*) preferences;
-(void) cleanUnusedTempImagesFromDocumentDirectory;
-(void) cleanAllTempImagesFromDocumentDirectory;
-(void) setNeedsRefreshForAllViews;

@end
