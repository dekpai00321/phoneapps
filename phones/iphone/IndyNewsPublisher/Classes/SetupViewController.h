/*
 SetupViewController class is view controller for the Setup page
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>
#import "IndyNewsViewController.h"

@interface SetupViewController : IndyNewsViewController<UIScrollViewDelegate> {

	IBOutlet UIScrollView *scrollView;
	
	IBOutlet UITextField *author;

	IBOutlet UITextField *additionalText;
	
	IBOutlet UITextField *relatedLink;
	
	IBOutlet UITextField *email;

	IBOutlet UISwitch *displayEmail;	
	
	/// defaults
	
	IBOutlet UITextField *defaultTitle;
	
	IBOutlet UISwitch *resetTitle;	
	
	IBOutlet UITextField *defaultAuthor;
	
	IBOutlet UISwitch *resetAuthor;	
	
	IBOutlet UITextField *defaultSummary;
	
	IBOutlet UISwitch *resetSummary;	
	
	//advanced
	
	IBOutlet UITextField *baseURL;
	
	IBOutlet UITextField *relativePublishLink;
	
	IBOutlet UISlider *maxNumberOfAttachments;
	
	IBOutlet UITextField *userKey;
	
	IBOutlet UILabel *maxNumberOfAttachmentsLabel;

}


-(IBAction) respondToSwitchChange:(id)sender;

- (void) setSliderLabelText;

//react to slider change
- (IBAction)maxNumSliderChanged:(id)sender;
@end
