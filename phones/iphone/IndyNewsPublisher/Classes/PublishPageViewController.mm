/*
 PublishPageViewController class is view controller for the publish page
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#import "PublishPageViewController.h"
#import "SetupViewController.h"
#import "ViewSiteViewController.h"
#import "optionlist.h"
#import "PhotoHelper.h"
#include "../../IndybayCppPublisher/src/controller/newsitempublisher.h"

@implementation PublishPageViewController


- (void)textFieldDidBeginEditing:(UITextView *)sender{
	[dismissKeyboardButton setHidden:TRUE];
	[clearKeyboardButton setHidden:TRUE];
}

- (void)textViewDidBeginEditing:(UITextView *)sender{
	[dismissKeyboardButton setHidden:FALSE];
	[clearKeyboardButton setHidden:FALSE];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (void)viewDidLoad {
	[super viewDidLoad];

	lastConnectSuccessful=false;
	isCurrentPickerRegion=false;
	actionSheetIsPublish=false;
	[dismissKeyboardButton setHidden:TRUE];
	[clearKeyboardButton setHidden:TRUE];
 	[self refreshFromCurrentItem];
	if(networkStatus==NotReachable){
		[publishButton setEnabled:NO]; 
		[activitySpinner stopAnimating];
		[publishButton setTitle:@"Offline" forState:UIControlStateNormal] ;
	}else {
		[self indicateConnectionSuccess];
	}
	isLoaded=true;
	
}

-(void)setupInitialValues{
	[super setupInitialValues];
	regionList=OptionList::newRegionList();
	topicList=OptionList::newTopicList();
}



-(void)refreshFromCurrentItem{
	if (needsRefresh){
		string title=[appDelegate newsItem]->getTitle();
		[titleText setText:[NSString stringWithUTF8String:title.c_str()]];
		[summaryTextView setText:[NSString stringWithUTF8String:[appDelegate newsItem]->getSummary().c_str()]];
		
		[imageCount setTitle:[NSString stringWithFormat:@"Number Of Attached Images: %d",[appDelegate newsItem]->getAttachments().size()] forState:UIControlStateNormal] ;
		
		
		NSString* currentRegionName=[NSString stringWithUTF8String:regionList->getOptionNameFromId([appDelegate newsItem]->getRegionId()).c_str()];
		if (currentRegionName!=nil){
			[regionButton setTitle:[NSString stringWithFormat:@"%@",currentRegionName] forState:UIControlStateNormal];
		}
		NSString* currentTopicName=[NSString stringWithUTF8String:topicList->getOptionNameFromId([appDelegate newsItem]->getTopicId()).c_str()];
		if (currentTopicName!=nil){
			[topicButton setTitle:[NSString stringWithFormat:@"%@",currentTopicName] forState:UIControlStateNormal];
		}
		needsRefresh=false;
	}
}


- (IBAction)hideKeyboardAction:(id)sender{  
	[dismissKeyboardButton setHidden: YES];
	[clearKeyboardButton setHidden: YES];
	[self refreshCurrentItemFromView];
	[summaryTextView resignFirstResponder];
}  

- (IBAction)clearKeyboardAction:(id)sender{  
	[summaryTextView setText:@""];
}  


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	
	[self refreshCurrentItemFromView];
	[textField resignFirstResponder];
	
	return NO;
}

-(void)refreshCurrentItemFromView{
	string newSummary=[[self trim:[summaryTextView text]] UTF8String];
	[appDelegate newsItem]->setSummary(newSummary);
	string newTitle=[[self trim:[titleText text]] UTF8String];
	[appDelegate newsItem]->setTitle(newTitle);
	[[self appDelegate]saveCurrentNewsItem];
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


-(void)indicateStartOfPublish{
	[titleText setEnabled:NO];
	summaryTextView.editable=NO;
	[publishButton setEnabled:NO]; 
	[activitySpinner startAnimating];
	[publishButton setTitle:@"Publishing" forState:UIControlStateNormal] ;
}


-(void)indicatePublishSuccess:(NSString*)urlOfResult{
	[appDelegate setNeedsRefreshForAllViews];
	[self refreshFromCurrentItem];
	if ([appDelegate newsItem]->getAttachments().size()==0){
		[appDelegate cleanAllTempImagesFromDocumentDirectory];
	}
	[self indicateEndOfPublish];
	[self resetPublishPage];
	self.tabBarController.selectedIndex = 3;
	[(ViewSiteViewController*)(self.tabBarController.selectedViewController) goTo:urlOfResult];
}

-(void)indicatePublishFailure:(NSString*)errorMessage{
	[self indicateEndOfPublish];
	[self displayErrorMessage:errorMessage];
}


-(void)indicateEndOfPublish{
	actionSheetIsPublish=false;
	sleep(1);
	[publishActionSheet dismissWithClickedButtonIndex:1 animated:TRUE];
	[publishActionSheet release];
	[self resetNewsItem];
	
}
-(void)indicateStartOfConnect{
	[publishButton setEnabled:NO]; 
	[activitySpinner startAnimating];
	[publishButton setTitle:@"Connecting" forState:UIControlStateNormal] ;
	
}

-(void)indicateConnectionSuccess{
	[self indicateEndOfConnect];
	lastConnectSuccessful=true;
}

-(void)indicateConnectionFailure:(NSString*)message{
	[self indicateEndOfConnect];
	[publishButton setTitle:@"Connect" forState:UIControlStateNormal] ;
	lastConnectSuccessful=false;
	[self displayErrorMessage:message];
}

-(void)indicateEndOfConnect{
	//page isnt done with initial load until connection test finishes
	isLoaded=true;
	[self resetPublishPage];
}

-(void)resetPublishPage{
	[titleText setEnabled:YES];
	summaryTextView.editable=YES;
	[publishButton setTitle:@"Publish" forState:UIControlStateNormal] ;	
	[activitySpinner stopAnimating];
	[publishButton setEnabled:YES]; 
}

-(IBAction)resetNewsItem{
	//do main part of refresh (includes attachments) and then force refresh of other fields (since this is user choosing a refresh not a publish)
	[appDelegate newsItem]->reset([appDelegate preferences]);
	if ([appDelegate preferences]->getResetTitle()){
		[appDelegate newsItem]->setTitle([appDelegate preferences]->getDefaultTitle());
	}
	if ([appDelegate preferences]->getResetSummary()){
		[appDelegate newsItem]->setSummary([appDelegate preferences]->getDefaultSummary());
	}

	[appDelegate saveCurrentNewsItem];
	[appDelegate setNeedsRefreshForAllViews];
	[self refreshFromCurrentItem];
}



- (IBAction)publishAction:(id)sender{  
	
	
	
	if (!lastConnectSuccessful){
		return;
	}
	
	actionSheetIsPublish=true;
	
	[self refreshCurrentItemFromView];
	
	WebHelper::bytesSentSoFarForCurrentPost=0;
	int attachNum=[appDelegate newsItem]->getAttachments().size();
	WebHelper::estimatedBytesToSendTotalForCurrentPost=1000+100000*attachNum;
	WebHelper::lastPOSTStartTime=time(NULL);
	
	publishActionSheet = [[UIActionSheet alloc] 
						  initWithTitle:@"Publishing..."
						  delegate: self
						  cancelButtonTitle:nil
						  destructiveButtonTitle:nil
						  otherButtonTitles:nil];
	publishActionSheet.actionSheetStyle=UIActionSheetStyleBlackOpaque;
	publishActionSheet.backgroundColor=[UIColor colorWithRed:0.2 green:0.2 blue:0.35 alpha:0.6];
	[publishActionSheet retain];
	
	// Add the progress bar
	
	progressBar= [[UIProgressView alloc] initWithFrame:CGRectMake(30,35,260, 10)];
	[progressBar setProgress:0.05];
	//[progressBar setBounds:CGRectMake(10,40,300, 10)];
	[publishActionSheet addSubview:progressBar];
	
	progressLabel= [[UILabel alloc]  initWithFrame:CGRectMake(0,55,320,175)];
	progressLabel.backgroundColor =[UIColor colorWithRed:0.4 green:0.4 blue:0.70 alpha:0.8];
	progressLabel.textColor =[UIColor colorWithRed:1 green:1 blue:1 alpha:0.9];
	progressLabel.numberOfLines=0;
	//progressLabel.font=[UIFont fontWithName:@"Courier" size:24];
	progressLabel.textAlignment= UITextAlignmentCenter;
	
	[progressLabel setText:@"Preparing News Item To Post"];
	
	//[progressLabel setBounds:CGRectMake(100,200,200, 100)];
	[publishActionSheet addSubview:progressLabel];
	
	[self startUpdatingProgress:nil];
	
	[publishActionSheet showInView:self.view];	
	[publishActionSheet setBounds:CGRectMake(0,0,320, 400)];
	postStartTime=time(NULL);
	[publishActionSheet release];
	[progressLabel release];
	[progressBar release];
}

-(IBAction)switchToImageTab:(id)sender{
	[self refreshCurrentItemFromView];
	self.tabBarController.selectedIndex = 2;
}

- (IBAction)choseRegion:(id)sender{
	isCurrentPickerRegion=true;
	[self choseRegionOrTopic:@"Select Region"];
}


- (IBAction)choseTopic:(id)sender{
	
	isCurrentPickerRegion=false;
	[self choseRegionOrTopic:@"Select Topic"];
}


- (void)choseRegionOrTopic:(NSString*)selectionTitle{
	
	[dismissKeyboardButton setHidden:TRUE];
	[clearKeyboardButton setHidden:TRUE];
	
	actionSheetIsPublish=false;
	
	[self refreshCurrentItemFromView];
	
	UIActionSheet *actionSheet = [[UIActionSheet alloc] 
								  initWithTitle:selectionTitle
								  delegate: self
								  cancelButtonTitle: @"Select"
								  destructiveButtonTitle:nil
								  otherButtonTitles:nil];
	
	// Add the picker
	
	pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,110,0,0)];
	
	pickerView.delegate = self;
	pickerView.showsSelectionIndicator = YES;    // note this is default to NO
	[actionSheet addSubview:pickerView];
	
	[actionSheet showInView:self.view];
	[actionSheet setBounds:CGRectMake(0,0,320, 600)];
	[actionSheet release];
	
}


- (void)didPresentActionSheet:(UIActionSheet *)actionSheet{
	
	if (actionSheetIsPublish){
		NewsItemPublisher* publisher= new NewsItemPublisher([appDelegate preferences]);
		vector<string> errors=publisher->publishNewsItem([appDelegate newsItem]);
		if (errors.size()>0){
			string error1=errors.at(0);
			[self indicatePublishFailure:[NSString  stringWithUTF8String:error1.c_str()]];
		}else{
			[appDelegate newsItem]->reset([appDelegate preferences]);
			[appDelegate saveCurrentNewsItem];
			[appDelegate setNeedsRefreshForAllViews];
			[self indicatePublishSuccess:[NSString  stringWithUTF8String:([appDelegate newsItem]->getPostedURL()+"?pda=true").c_str()]];
		}
	}else{
		NSInteger selectedRow=0;
		if (isCurrentPickerRegion){
			selectedRow=regionList->getRowNumFromId([appDelegate newsItem]->getRegionId());
		}else{
			selectedRow=topicList->getRowNumFromId([appDelegate newsItem]->getTopicId());
		}
		[pickerView selectRow:selectedRow inComponent:0 animated:NO];
	}
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	if ([appDelegate newsItem]->isDirty()){
		[appDelegate saveCurrentNewsItem];
	}
	[self refreshFromCurrentItem];
}

-(void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (actionSheetIsPublish)
		actionSheetIsPublish=false;
	[pickerView release];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
	if (isCurrentPickerRegion)
		return [NSString stringWithUTF8String:regionList->optionList.at(row).name.c_str()];
	else 
		return [NSString stringWithUTF8String:topicList->optionList.at(row).name.c_str()];
	
	
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	if (isCurrentPickerRegion)
		return regionList->optionList.size();
	else
		return topicList->optionList.size();
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
	if (isCurrentPickerRegion){
		int regionId=regionList->optionList.at(row).id;
		[appDelegate newsItem]->setRegionId(regionId);
	}
	else{
		int newTopicId=topicList->optionList.at(row).id;
		[appDelegate newsItem]->setTopicId(newTopicId);
	}
	needsRefresh=true;
	
}



-(void)startUpdatingProgress:(id)param{
	NSAutoreleasePool	 *autoreleasepool = [[NSAutoreleasePool alloc] init];

	if (actionSheetIsPublish){
		if (actionSheetIsPublish ){
			[self updateProgress];
			sleep(1);
			if ( actionSheetIsPublish){
				[NSThread detachNewThreadSelector:@selector(startUpdatingProgress:) toTarget:self withObject:nil];
			}
		}
	}
	[autoreleasepool release];
}


- (void) updateProgress{
	//printf("received notification!!\n");
	if (actionSheetIsPublish){
		NSAutoreleasePool	 *autoreleasepool = [[NSAutoreleasePool alloc] init];
		//printf("testing 2:%d\n", round(WebHelper::estimatedPostTimeLeft));
		if (WebHelper::bytesSentSoFarForCurrentPost>1000 && WebHelper::estimatedBytesToSendTotalForCurrentPost>0){
			double percentSent=  (double)WebHelper::bytesSentSoFarForCurrentPost/(double)WebHelper::estimatedBytesToSendTotalForCurrentPost;
			
			int bytesLeftToSend=WebHelper::estimatedBytesToSendTotalForCurrentPost-WebHelper::bytesSentSoFarForCurrentPost;
			time_t timeSoFar=time(NULL)-WebHelper::lastPOSTStartTime;
			if (timeSoFar>0){
				double postSpeedBPS=WebHelper::bytesSentSoFarForCurrentPost/timeSoFar;
				if (postSpeedBPS>0){
					double estimatedSendTime=3+(bytesLeftToSend/postSpeedBPS);
					
					[progressBar setProgress:percentSent];
					
					NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
					[numberFormatter setGroupingSize:3];
					[numberFormatter setGroupingSeparator:@","];
					[numberFormatter setUsesGroupingSeparator:YES];
					
					NSNumber *bytesSent = [NSNumber numberWithInt:WebHelper::bytesSentSoFarForCurrentPost];
					NSNumber *bytesLeft = [NSNumber numberWithInt:WebHelper::estimatedBytesToSendTotalForCurrentPost-WebHelper::bytesSentSoFarForCurrentPost];
					
					NSString* publishMessage=@"";
					if (estimatedSendTime<3.5){
						publishMessage= [NSString stringWithFormat:@"Finished posting.\nWaiting on server response"];
					}else if (estimatedSendTime<75){
						publishMessage= [NSString stringWithFormat:@"Bytes Sent: %@\nBytes Remaining:%@\n\n~%.0f seconds remaining",
										 [numberFormatter stringFromNumber:bytesSent],
										 [numberFormatter stringFromNumber:bytesLeft],
										 estimatedSendTime];
					}else if (estimatedSendTime<600) {
						publishMessage= [NSString stringWithFormat:@"Bytes Sent: %@\nBytes Remaining:%@\n\n~%.1f minutes remaining",
										 [numberFormatter stringFromNumber:bytesSent],
										 [numberFormatter stringFromNumber:bytesLeft],
										 estimatedSendTime/60];
					}else{
						publishMessage= @"Publishing: unable to estimate time to post";
					}
					//progressLabel.fontSize=[UIFont fontWithName:@"Courier" size:14];
					[progressLabel setText:publishMessage];
				}
			}
		}
		[autoreleasepool release];
	}
	
}

-(void)updateBasedOffReachability: (NetworkStatus)status isCurrentViewController:(bool)isCurrent{
	NetworkStatus oldStatus=networkStatus;
	[super updateBasedOffReachability:status isCurrentViewController:isCurrent];
	if(isLoaded){
		if(status==NotReachable){
			[publishButton setEnabled:NO]; 
			[activitySpinner stopAnimating];
			[publishButton setTitle:@"Offline" forState:UIControlStateNormal] ;
		}else if (oldStatus==NotReachable) {
			[self indicateConnectionSuccess];
		}
	}
	
}





@end
