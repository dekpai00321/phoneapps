/*
 PhotoHelper contains image resizing related functions used by the app
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <Foundation/Foundation.h>

typedef enum {
	DontResize = 0,
	ThumbnailSize,
	SmallSize,
	MediumSize,
	LargeSize
} PhotoResizeScale;


@interface PhotoHelper : NSObject {

}

-(CGSize) cgsizeFromResizeAmount:(PhotoResizeScale)amount;
-(UIImage*)scaleAndRotateImage:(UIImage*)data resizeAmount:(PhotoResizeScale)resizeAmount originalOrientation:(UIImageOrientation)orientation;
- (UIImage *)scaleAndRotateImage:(UIImage *)image absoluteSize:(CGSize)newSize originalOrientation:(UIImageOrientation)orientation;
@end
