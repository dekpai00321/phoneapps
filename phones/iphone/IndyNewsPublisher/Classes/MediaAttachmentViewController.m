/*
 MediaAttachmentViewController class is the view controller or the single item view page (the page showing a single attachment)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "MediaAttachmentViewController.h"


@implementation MediaAttachmentViewController

@synthesize currentImage;

-(MediaAttachmentViewController*)init{
	self=[super init];
	//[self setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
	return self;
}


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	UIView* newView=[[UIView alloc]init];
	self.view=newView;
	[newView release];
	self.view.frame=[UIScreen mainScreen].bounds;
	self.view .backgroundColor = [UIColor blackColor];
	[self.view setMultipleTouchEnabled:YES];
}

- (void)displayImageView
{
	
	CGRect initialBounds=self.view.bounds;
    CGRect scrollFrame =    CGRectMake(0,41.0f,  initialBounds.size.width, initialBounds.size.height-41.0f);
	
	UINavigationBar* navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 40.0f)];
	//[navBar setBarStyle:5]; 
	[navBar setDelegate:self];
	UINavigationItem* navItem= [[UINavigationItem alloc] initWithTitle:@"Attachments"];
	[navBar pushNavigationItem:navItem animated:NO];
	[navItem release];
	navItem= [[UINavigationItem alloc] initWithTitle:@"View Image"];
	[navBar pushNavigationItem:navItem animated:NO];
	[navItem release];
    scrollView = [[UIScrollView alloc] initWithFrame:scrollFrame] ;
	
	[scrollView setBackgroundColor:[UIColor blackColor]];
	[scrollView setCanCancelContentTouches:NO];
	scrollView.clipsToBounds = YES;    // default is NO, we want to restrict drawing within our scrollview
	scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	currentImageView = [[UIImageView alloc] initWithImage:currentImage];
	[scrollView addSubview:currentImageView];
	[scrollView setContentSize:CGSizeMake(currentImageView.frame.size.width, currentImageView.frame.size.height)];
	
	float horizontalScale=scrollFrame.size.width /currentImage.size.width;
	float verticalScale=scrollFrame.size.height /currentImage.size.height;
	
	scrollView.minimumZoomScale = MIN(MIN(horizontalScale,verticalScale)*0.90,0.9);
	scrollView.maximumZoomScale = MAX(MAX(horizontalScale,verticalScale)*1.1,1.1);
	scrollView.delegate = self;
	[scrollView setZoomScale: MAX(horizontalScale,verticalScale) animated:NO];
	
	[scrollView setScrollEnabled:YES];
	
    [[self view] addSubview:scrollView];
	[scrollView release];
	[[self view]addSubview:navBar];
	[navBar release];
}


- (void)navigationBar:(UINavigationBar *)navigationBar didPopItem:(UINavigationItem *)item{
	[self closeView];
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return currentImageView;
}


- (void)closeView{
	if (currentImageView!=nil) {
		[currentImageView removeFromSuperview];
		[currentImageView release];
		currentImageView=nil;
	}
	//[[UIApplication sharedApplication] setStatusBarHidden:NO];
	[self dismissModalViewControllerAnimated:NO];

}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self displayImageView];
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
