/*
 SetupViewController class is view controller for the Setup page
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "SetupViewController.h"


@implementation SetupViewController


- (void)viewDidLoad {
	NSLog(@"SetupViewController Loaded");
	needsRefresh=true;
	scrollView.clipsToBounds = YES;
	scrollView.delegate = self;
	scrollView.alwaysBounceVertical;
	scrollView.scrollEnabled = YES;
	[scrollView
	 setContentSize:CGSizeMake(scrollView.frame.size.width,
							   scrollView.frame.size.height+500)];
//	[scrollView ]
}

//respond to end of editing of news item
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return NO;
}

//respond to editing of text items by updating news item and saving
- (void)textFieldDidEndEditing:(UITextField *)textField{
	[self refreshCurrentItemFromView];
}

//implementation
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 21;
    [scrollView setContentOffset:pt animated:YES];           
}


//refresh news item object from view and then save
-(void)refreshCurrentItemFromView{
	[super refreshCurrentItemFromView];
		//current news item values
	
	string newAuthor=[[self trim:[author text]] UTF8String];
	[appDelegate newsItem]->setAuthor(newAuthor);
	string newText=[[self trim:[additionalText text]] UTF8String];
	[appDelegate newsItem]->setText(newText); 

	string newRelatedLink=[[self trim:[relatedLink text]] UTF8String];
		[appDelegate newsItem]->setRelatedLink(newRelatedLink);
	
	string newEmail=[[self trim:[email text]] UTF8String];
		[appDelegate newsItem]->setEmail(newEmail);
	 
	bool newDisplayEmail=[displayEmail isOn];
	[appDelegate newsItem]->setDisplayEmail(newDisplayEmail);
	
	[[self appDelegate]saveCurrentNewsItem];
	
	string newDefaultTitle=[[self trim:[defaultTitle text]] UTF8String];
	[appDelegate preferences]->setDefaultTitle(newDefaultTitle);
	if ([resetTitle isOn]){
		[appDelegate preferences]->setResetTitle(true);
	}else{
		[appDelegate preferences]->setResetTitle(false);
	}
	
	string newDefaultSummary=[[self trim:[defaultSummary text]] UTF8String];
	[appDelegate preferences]->setDefaultSummary(newDefaultSummary);
	if ([resetTitle isOn]){
		[appDelegate preferences]->setResetSummary(true);
	}else{
		[appDelegate preferences]->setResetSummary(false);
	}
	
	string newBasedURL=[[self trim:[baseURL text]] UTF8String];
	[appDelegate preferences]->setBaseURL(newBasedURL);
	
	string newRelativePublishPageLink=[[self trim:[relativePublishLink text]] UTF8String];
	[appDelegate preferences]->setRelativePublishPageURLPath(newRelativePublishPageLink);
	
	int newMaxNumberAttachments=[maxNumberOfAttachments value];
	[appDelegate preferences]->setMaxFilesAllowed(newMaxNumberAttachments);

	string newUserKey=[[self trim:[userKey text]] UTF8String];
	[appDelegate preferences]->setPersonalKey(newUserKey);
	
	[[self appDelegate]savePreferences];
	
}

//on email display change refresh current item from view 
-(IBAction) respondToSwitchChange:(id)sender{
	[self refreshCurrentItemFromView];
}

//refreshes the view from the current news item
-(void)refreshFromCurrentItem{
	
	//if the page is refreshed scroll to top even if nothing has changed
	CGPoint pt;
    pt.x = 0;
    pt.y = 0;
	[scrollView setContentOffset:pt animated:NO];   

	
	if (needsRefresh){
		//current item values
		[author setText:[NSString stringWithUTF8String:[appDelegate newsItem]->getAuthor().c_str()]];
		[additionalText  setText:[NSString stringWithUTF8String:[appDelegate newsItem]->getText().c_str()]];
		[relatedLink setText:[NSString stringWithUTF8String:[appDelegate newsItem]->getRelatedLink().c_str()]];
		[email setText:[NSString stringWithUTF8String:[appDelegate newsItem]->getEmail().c_str()]];
		[displayEmail setOn:[appDelegate newsItem]->getDisplayEmail()];

		//defaults
		[defaultTitle setText:[NSString stringWithUTF8String:[appDelegate preferences]->getDefaultTitle().c_str()]];
		[resetTitle setOn:[appDelegate preferences]->getResetTitle()];

		[defaultSummary setText:[NSString stringWithUTF8String:[appDelegate preferences]->getDefaultSummary().c_str()]];
		[resetSummary setOn:[appDelegate preferences]->getResetSummary()];
		
		//advances
		[baseURL setText:[NSString stringWithUTF8String:[appDelegate preferences]->getBaseURL().c_str()]];
		[relativePublishLink setText:[NSString stringWithUTF8String:[appDelegate preferences]->getRelativePublishPageURLPath().c_str()]];
		[userKey setText:[NSString stringWithUTF8String:[appDelegate preferences]->getPersonalKey().c_str()]];
		[maxNumberOfAttachments setValue:[appDelegate preferences]->getMaxFilesAllowed()];
		[self setSliderLabelText];
	
	}
	
}

- (void) setSliderLabelText{
	maxNumberOfAttachmentsLabel.text=
	[NSString stringWithFormat:@"Max # Attachments: %d",[appDelegate preferences]->getMaxFilesAllowed()];
}

- (IBAction)maxNumSliderChanged:(id)sender{
	maxNumberOfAttachments.value=round(maxNumberOfAttachments.value);
	[self refreshCurrentItemFromView];
	[appDelegate setNeedsRefreshForAllViews];
	[self setSliderLabelText];
	
}

@end
