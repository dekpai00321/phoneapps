/*
 IndyNewsPublisherAppDelegate is the main controller class for the app
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "IndyNewsPublisherAppDelegate.h"
#import "ViewSiteViewController.h"
#import "IndyNewsViewController.h"
#import "../../IndybayCppPublisher/src/persistence/filehelper.h"

@implementation IndyNewsPublisherAppDelegate

@synthesize window;
@synthesize tabBarController;

- (NewsItem*) newsItem{
	return currentItemToPublish;
}

- (IndyPreferences*) preferences{
	return preferences;
}


- (IndyNewsPublisherAppDelegate*)init{
	self=[super init];
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	//load preferences
	preferences= new IndyPreferences();
	NSString *prefsAsString = [userDefaults stringForKey:@"preferences"];
	if (prefsAsString!=nil && [prefsAsString length]>0){
		string str3=[prefsAsString UTF8String];
		//NSLog(@"loading current news item");
		
		if (str3!="nil"){
			preferences->loadFromString(str3);
		}
	}
	
	//start reachability checks in background
	NSString* baseURL= [NSString stringWithUTF8String:preferences->getBaseURL().c_str()];
	serverReachability = [[Reachability reachabilityWithHostName: baseURL] retain];
	serverReachable = [serverReachability currentReachabilityStatus];


	return self;
}


//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	serverReachable = [serverReachability currentReachabilityStatus];
	
	for (int i=0;i<[tabBarController.viewControllers count];i++){
		IndyNewsViewController* nextViewController=(IndyNewsViewController*)[tabBarController.viewControllers objectAtIndex:i];
		[nextViewController updateBasedOffReachability:serverReachable isCurrentViewController:(i==tabBarController.selectedIndex)];
	}
	
}

//after application finished loading
- (void)applicationDidFinishLaunching:(UIApplication *)application {
    
	NSLog(@"applicationDidFinishLaunching started");
	
	// Add the tab bar controller's current view as a subview of the window
    [window addSubview:tabBarController.view];
		
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

	//load news item
	currentItemToPublish= new NewsItem(preferences);
	NSString *currentNewsItemAsString = [userDefaults stringForKey:@"currentNewsItem"];
	if (currentNewsItemAsString!=nil && [currentNewsItemAsString length]>0){
		string str2=[currentNewsItemAsString UTF8String];
		//NSLog(@"loading current news item");
		
		if (str2!="nil"){
			currentItemToPublish->loadFromString(str2);
		}
	}
	//	printf("\n*************\nAuthor names Was %s\n", currentItemToPublish.author.c_str());
	
	[self cleanUnusedTempImagesFromDocumentDirectory];
	
	bool isCurrent=true;
	int currentTab=0;
	if (currentItemToPublish->getAuthor()!=""){
		currentTab=1;
	}
	
	//refresh pages
	for (int i=0;i<[tabBarController.viewControllers count];i++){
		IndyNewsViewController* nextViewController=(IndyNewsViewController*)[tabBarController.viewControllers objectAtIndex:i];
		nextViewController.appDelegate=self;
		[nextViewController setupInitialValues];
		nextViewController.needsRefresh=true;
		if (i==currentTab){
		 isCurrent=true;
		}else {
			isCurrent=false;	
		}
		[nextViewController refreshFromCurrentItem];
		[nextViewController updateBasedOffReachability:serverReachable isCurrentViewController:isCurrent];
	}
	
		
	//listen for reachability notifications
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	[serverReachability startNotifer];	
	
	tabBarController.selectedIndex = currentTab;
	NSLog(@"applicationDidFinishLaunching finished");
	
	
}



// Optional UITabBarControllerDelegate method
- (void)tabBarController:(UITabBarController *)tabBarController2 didSelectViewController:(UIViewController *)viewController {
	if (((IndyNewsViewController*)viewController).needsRefresh){
		[(IndyNewsViewController*)viewController refreshFromCurrentItem];
	}
	[(IndyNewsViewController*)viewController tabBarItemJustSelected];
}


// Optional UITabBarControllerDelegate method
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
	NSLog(@"didEndCustomizingViewControllers called");
	
}

//show indybay web page
- (IBAction)indymediaIconClick:(id)sender {
	tabBarController.selectedIndex = 3;
	[(ViewSiteViewController*)tabBarController.selectedViewController goHome:self];
	
}

//free allocted objects
- (void)dealloc {
    [tabBarController release];
    [window release];
    [super dealloc];
}



/**
 applicationWillTerminate: saves changes in the application's managed object context before the application terminates.
 */
- (void)applicationWillTerminate:(UIApplication *)application {
	
}


#pragma mark -
#pragma mark Core Data stack


/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


//save newsItem to NSUserDefaults
-(void)saveCurrentNewsItem{
	if(currentItemToPublish->isDirty()){
		NSLog(@"saveCurrentNewsItem called and dirty bit was true");
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		const char* newsItemAsCStr=currentItemToPublish->toString().c_str();
		NSString* newsItemAsString=[NSString stringWithUTF8String:newsItemAsCStr];
		[userDefaults setObject:newsItemAsString forKey:@"currentNewsItem"];
		[userDefaults synchronize];
		currentItemToPublish->savedSuccessfully();
		NSLog(@"saveCurrentNewsItem finished");
	}
}	

//save preferencs to NSUserDefaults
-(void)savePreferences{
	if(preferences->isDirty()){
		NSLog(@"savePreferences called and dirty bit was true");
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		const char* newsItemAsCStr=preferences->toString().c_str();
		NSString* prefsAsString=[NSString stringWithUTF8String:newsItemAsCStr];
		[userDefaults setObject:prefsAsString forKey:@"preferences"];
		[userDefaults synchronize];
		preferences->savedSuccessfully();
	}
}	

//delete all images from temp folder
-(void) cleanAllTempImagesFromDocumentDirectory{
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentsDirectory = [paths objectAtIndex:0];
	FileHelper fileHelper;
	fileHelper.deleteFilesInDirectoryByExtension([documentsDirectory UTF8String], ".png");
}

//delete all images from temp folder that are not attachments on current order
-(void) cleanUnusedTempImagesFromDocumentDirectory{
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentsDirectory = [paths objectAtIndex:0];
	FileHelper fileHelper;
	vector<string> filesToDelete;
	filesToDelete=fileHelper.listFilesInDirectoryByExtension([documentsDirectory UTF8String], ".png");
	int success;
	bool deleteNext;
	for (int i=0;i<filesToDelete.size();i++){
		deleteNext=true;
		for (int j=0;j<currentItemToPublish->getAttachments().size();j++){
			if (currentItemToPublish->getAttachments().at(j).filePath==filesToDelete.at(i)){
				deleteNext=false;
				break;
			}
		}
		if (deleteNext){
			success=remove(filesToDelete.at(i).c_str());
		}
	}
	
}


-(void) setNeedsRefreshForAllViews{
	//refresh pages
	for (int i=0;i<[tabBarController.viewControllers count];i++){
		IndyNewsViewController* nextViewController=(IndyNewsViewController*)[tabBarController.viewControllers objectAtIndex:i];
		nextViewController.needsRefresh=true;
	}
	
}


@end

