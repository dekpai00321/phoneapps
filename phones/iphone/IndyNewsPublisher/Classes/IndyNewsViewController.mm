/*
 IndyNewsViewController class is the parent class for all view controllers
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "IndyNewsViewController.h"


@implementation IndyNewsViewController

@synthesize appDelegate;
@synthesize needsRefresh;

-(void)setupInitialValues{
	errorAlert=nil;
	isInitialAlert=true;
	needsRefresh=true;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	needsRefresh=true;
}
	
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


//-(void)setCurrentItemToPublish:(NewsItem*)newItem{
//	currentItemToPublish=newItem;
//}

//-(NewsItem* )getCurrentItemToPublish{
//	return currentItemToPublish;
//}



-(void)tabBarItemJustSelected{
	[self refreshFromCurrentItem];
}

-(void)refreshFromCurrentItem{
	NSLog(@"refreshFromCurrentItem called ");
}

-(void)refreshCurrentItemFromView{
	NSLog(@"refreshCurrentItemFromView called ");
}

-(NSString*)trim:(NSString*)stringToTrim{
	return [stringToTrim stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}




- (void)displayErrorMessage:(NSString*) message{
	
	if(errorAlert!=nil){
		[errorAlert dismissWithClickedButtonIndex:0 animated:NO];
	}
	
	errorAlert = [[UIAlertView alloc] 
						  initWithTitle: nil 
						  message: message
						  delegate: self
						  cancelButtonTitle: @"ok"
						  otherButtonTitles: nil];
	[errorAlert show];
	[errorAlert release];
	
}

//set errorAlert to nil when it is closed 
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
	errorAlert=nil;
}



- (void)displayValidationMessage:(NSString*) message{
	
	UIAlertView *alert = [[UIAlertView alloc] 
						  initWithTitle: @"" 
						  message: message
						  delegate: self
						  cancelButtonTitle: @"ok"
						  otherButtonTitles: nil] ;
	[alert show];
	[alert release];

	
}

-(void)updateBasedOffReachability: (NetworkStatus)status isCurrentViewController:(bool)isCurrent{
	if(status==NotReachable && isCurrent){
		[self displayErrorMessage:@"Unable To Connect To Server. You Can Still Enter Your News Item But Will Not Be Able To Post."];
	}else if(networkStatus==NotReachable && status!=NotReachable && isCurrent && !isInitialAlert){
		[self displayErrorMessage:@"Established Connection To Server. You Should Now Be Able To Post."];
	}
	isInitialAlert=false;
	networkStatus=status;
}



@end
