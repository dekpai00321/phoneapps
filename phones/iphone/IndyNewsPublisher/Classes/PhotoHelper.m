/*
 PhotoHelper contains image resizing related functions used by the app
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "PhotoHelper.h"


@implementation PhotoHelper


//return CGSize for given size type
-(CGSize) cgsizeFromResizeAmount:(PhotoResizeScale)amount{
	
	CGSize		newSize = { 0, 0};
	switch(amount){	
		case ThumbnailSize:
			newSize.width=100;
			newSize.height=75;
			break;
		case SmallSize:
			newSize.width=400;
			newSize.height=300;
			break;
		case MediumSize:
			newSize.width=800;
			newSize.height=600;
			break;
		case LargeSize:
			newSize.width=1600;
			newSize.height=1200;
			break;
		default:
			break;
	}
	return newSize;
}


//resize to a given size name
-(UIImage*)scaleAndRotateImage:(UIImage*)image resizeAmount:(PhotoResizeScale)resizeAmount originalOrientation:(UIImageOrientation)orientation{
	return [self scaleAndRotateImage:image absoluteSize:[self cgsizeFromResizeAmount:resizeAmount] originalOrientation:orientation];
}


//resize to a size defined by a CGSize
- (UIImage *)scaleAndRotateImage:(UIImage *)image absoluteSize:(CGSize)newSize originalOrientation:(UIImageOrientation)orient{
	if (image==nil)
		return nil;
	//int kMaxResolution = 320; // Or whatever
	
	CGImageRef imgRef = [image CGImage];
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	CGFloat desiredWidth=newSize.width;
	CGFloat desiredHeight=newSize.height;
	
	//if sizeways one way or the other
	if (orient> UIImageOrientationDownMirrored){
		desiredWidth=newSize.height;
		desiredHeight=newSize.width;
	}
	
	
	CGAffineTransform transform = CGAffineTransformIdentity;
	
	
	CGRect bounds = CGRectMake(0, 0, desiredWidth, desiredHeight);
	
	
	CGFloat scaleRatio = desiredWidth / width;
	
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
	CGFloat boundHeight;
	
	
	transform = CGAffineTransformIdentity;
	
	switch(orient) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationLeft: //EXIF = 6
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationRightMirrored: //EXIF = 7
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		case UIImageOrientationRight: //EXIF = 8
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		default:
			[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
	}
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -scaleRatio, scaleRatio);
		CGContextTranslateCTM(context, -height, 0);
	}
	else {
		CGContextScaleCTM(context, scaleRatio, -scaleRatio);
		CGContextTranslateCTM(context, 0, -height);
	}
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return imageCopy;
}

@end
