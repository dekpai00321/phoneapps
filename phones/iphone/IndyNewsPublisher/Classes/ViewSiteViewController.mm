/*
 ViewSiteViewController class is view controller for the browser like page for the site (showing either mobile site or result of posting)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#import "ViewSiteViewController.h"

@implementation ViewSiteViewController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	NSLog(@"ViewSiteViewController loaded");
    [super viewDidLoad];
	[self goHome:self];
}

//loads the web page given a URL
- (void)goTo:(NSString*)url{
	NSLog(@"goto Called with %@",url);
	//Create a URL object.
	NSURL *urlObj = [NSURL URLWithString:url];
	
	//URL Requst Object
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:urlObj];
	
	//Load the request in the UIWebView.
	[webView loadRequest:requestObj];
	
}

- (IBAction)goHome:(id)sender {
	NSLog(@"go Home Called");
	[self goTo:[NSString stringWithFormat:@"http://%@/mobile/index.php",[NSString  stringWithUTF8String:[appDelegate preferences]->getBaseURL().c_str()]]];

}


@end
