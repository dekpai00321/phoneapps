/*
 IndyNewsViewController class is the parent class for all view controllers
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "IndyNewsPublisherAppDelegate.h"

@interface IndyNewsViewController : UIViewController {
	IndyNewsPublisherAppDelegate* appDelegate;
	NetworkStatus networkStatus;
	bool isInitialAlert;
	bool needsRefresh;
	UIAlertView *errorAlert;
}

-(void)refreshFromCurrentItem;

-(void)refreshCurrentItemFromView;

-(void)tabBarItemJustSelected;

-(void)setupInitialValues;

-(NSString*)trim:(NSString*)stringToTrim;

- (void)displayErrorMessage:(NSString*) message;

- (void)displayValidationMessage:(NSString*) message;

- (void)updateBasedOffReachability: (NetworkStatus)status isCurrentViewController:(bool)isCurrent;

@property (nonatomic, retain) IBOutlet IndyNewsPublisherAppDelegate *appDelegate;
@property bool needsRefresh;

@end
