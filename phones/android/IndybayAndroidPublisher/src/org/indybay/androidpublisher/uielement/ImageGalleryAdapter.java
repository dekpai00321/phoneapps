/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.uielement;

import org.indybay.androidpublisher.media.AndroidMediaHelper;
import org.indybay.androidpublisher.media.ImageHelper;
import org.indybay.androidpublisher.viewcontroller.AndroidPublisherController;
import org.indybay.androidpublisher.viewcontroller.R;
import org.indybay.publishhelper.model.NewsItemAttachment;

import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

/**
 * This is a custom UI object to show the scrolling thumbnails on the
 * attachments page It is used within a Gallery setup in the xml layout
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class ImageGalleryAdapter extends BaseAdapter {

	/**
	 * context is needed for some aspects of image manipulation
	 */
	private final Activity context;

	/**
	 * resource id of backrgound for scrolling cells in Gallery
	 */
	private final int itemBackground;

	/**
	 * Constructor sets the context and background
	 * 
	 * @param context
	 */
	public ImageGalleryAdapter(final Activity c) {

		this.context = c;

		final TypedArray a = c
				.obtainStyledAttributes(R.styleable.image_gallery1);
		this.itemBackground = a.getResourceId(
				R.styleable.image_gallery1_android_galleryItemBackground, 0);
		a.recycle();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * Sets the numbers of items to be displayed
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {

		if ((AndroidPublisherController.newsItem == null)
				|| (AndroidPublisherController.newsItem.getAttachments() == null)) {
			Log.e("ImageGalleryAdapter.getCount", "null newsitem");
			return 0;
		}

		Log.w("ImageGalleryAdapter.getCount", "Returning count "
				+ AndroidPublisherController.newsItem.getAttachments().size());
		return AndroidPublisherController.newsItem.getAttachments().size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * Gets the item at a given position
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(final int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * Gets the item id at a given position
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(final int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * gets a view for an individual image in the gallery
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	public View getView(final int position, final View convertView,
			final ViewGroup parent) {

		final ImageView imageView = new ImageView(this.context);
		if ((AndroidPublisherController.newsItem == null)
				|| (AndroidPublisherController.newsItem.getAttachments() == null)
				|| (position > AndroidPublisherController.newsItem
						.getAttachments().size())) {
			Log.e("ImageGalleryAdapter", "bad position or null newsitem");
			return imageView;
		}

		// final byte[] data =
		// ((NewsItemAttachment)AndroidPublisherController.newsItem
		// .getAttachments().get(position)).getThumbnailData();

		byte[] data = null;

		try {
			final AndroidMediaHelper mediaHelper = new AndroidMediaHelper(
					this.context);
			data = mediaHelper
					.getThumbnailDataGivenParentFilePath(((NewsItemAttachment) AndroidPublisherController.newsItem
							.getAttachments().get(position)).getFilePath());
		} catch (final Exception e) {
			Log.e("ImageGalleryAdapter", "getView", e);
		}
		final Bitmap imageBmp = ImageHelper.getBmpFromData(data);
		imageView.setImageBitmap(imageBmp);
		imageView.setScaleType(ImageView.ScaleType.FIT_XY);
		imageView.setLayoutParams(new Gallery.LayoutParams(200, 200));
		imageView.setBackgroundResource(this.itemBackground);
		return imageView;
	}
}