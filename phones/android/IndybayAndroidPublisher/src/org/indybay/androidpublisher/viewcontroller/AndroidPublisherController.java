/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.viewcontroller;

import java.io.IOException;

import org.indybay.androidpublisher.media.AndroidMediaHelper;
import org.indybay.androidpublisher.model.AndroidPreferences;
import org.indybay.androidpublisher.persistence.AndroidNewsItemPersister;
import org.indybay.publishhelper.model.NewsItem;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Parent for the other 3 view controllers
 * 
 * @author Zogren for Indybay.org
 * 
 */
public abstract class AndroidPublisherController extends Activity {

	/**
	 * news item cached between instances and shared across pages
	 */
	public static NewsItem newsItem = null;

	/**
	 * site name label that appears on call pages
	 */
	private TextView siteNameLabel = null;

	protected String baseFilePathForNewsItem = null;

	/**
	 * media helper is needed by all pages and some common functionality
	 */
	protected AndroidMediaHelper mediaHelper = null;

	/**
	 * the current preferences
	 */
	protected AndroidPreferences preferences = null;

	/*
	 * (non-Javadoc)
	 * 
	 * common functionality when any page loads
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {

		this.baseFilePathForNewsItem = this.getCacheDir().getAbsolutePath();
		this.preferences = new AndroidPreferences(this);

		this.mediaHelper = new AndroidMediaHelper(this);

		super.onCreate(savedInstanceState);

		try {
			final AndroidNewsItemPersister persister = new AndroidNewsItemPersister(
					this.baseFilePathForNewsItem);
			if (AndroidPublisherController.newsItem == null) {
				AndroidPublisherController.newsItem = persister
						.loadFromFileOrInitFromPrefs(this.preferences);
			}
		} catch (final Exception e) {
			Log.e("AndroidPublisherController", "onCreate", e);
			e.printStackTrace();
		}

		this.siteNameLabel = (TextView) this
				.findViewById(R.id.common_page_header_label);
		if (this.siteNameLabel != null) {
			this.siteNameLabel.setText(this.preferences.getBaseURL());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * sets up the menu that appears on all pages
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = this.getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * Handles selections from the menu
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		Intent myIntent = null;
		switch (item.getItemId()) {
		case R.id.menu_item_publish_page:
			this.refreshNewsItemFromForm();
			myIntent = new Intent(this, PublishPageController.class);
			this.startActivity(myIntent);
			return true;
		case R.id.menu_item_attachment_page:
			this.refreshNewsItemFromForm();
			myIntent = new Intent(this, PhotoListPageController.class);
			this.startActivity(myIntent);
			return true;
		case R.id.menu_item_add_saved:

			this.refreshNewsItemFromForm();
			if (AndroidPublisherController.newsItem.getAttachments().size() >= this.preferences
					.getMaxFilesAllowed()) {
				this
						.displayInfo("You Have Already Selected The Maximum Number Of Files Allowed");
				return true;
			}
			PhotoListPageController.forceSelectImage = true;
			myIntent = new Intent(this, PhotoListPageController.class);
			this.startActivity(myIntent);
			return true;

		case R.id.menu_item_setup_page:
			this.refreshNewsItemFromForm();
			myIntent = new Intent(this, SetupPageController.class);
			this.startActivity(myIntent);
			return true;

		case R.id.menu_item_view_site:

			this.refreshNewsItemFromForm();
			myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"
					+ this.preferences.getBaseURL() + "/mobile/index.php"));
			this.startActivity(myIntent);
			return true;

		case R.id.menu_item_reset:
			AndroidPublisherController.newsItem = null;
			if (AndroidPublisherController.newsItem == null) {
				AndroidPublisherController.newsItem = new NewsItem(
						this.preferences);
			} else {
				AndroidPublisherController.newsItem.reset(this.preferences);
			}
			try {
				final AndroidNewsItemPersister persister = new AndroidNewsItemPersister(
						this.baseFilePathForNewsItem);
				persister.save(AndroidPublisherController.newsItem);
			} catch (final IOException e) {
				Log.e("AndroidPublisherController", "onOptionsItemSelected", e);
			}
			myIntent = new Intent(this, PublishPageController.class);
			this.startActivity(myIntent);
			return true;
		}
		return false;
	}

	/**
	 * Display an error message in a popup
	 * 
	 * @param message
	 */
	protected void displayError(final String message) {
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Error");
		alertDialog.setMessage(message);
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int which) {
				return;
			}
		});
		alertDialog.show();
	}

	/**
	 * Displays a non-error message to the user in a popup
	 * 
	 * @param message
	 */
	protected void displayInfo(final String message) {
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Info");
		alertDialog.setMessage(message);
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int which) {
				return;
			}
		});
		alertDialog.show();
	}

	/**
	 * Fill in page from saved data
	 */
	protected abstract void refreshFormFromNewsItem();

	/**
	 * Save changed data from form to file
	 */
	protected abstract void refreshNewsItemFromForm();

}
