/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.media;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.indybay.androidpublisher.viewcontroller.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Android specific video manipulation methods (not much here yet since
 * thumbnails were added post 1.6 and for now we need to support as far back as
 * at least 1.5)
 * 
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class VideoHelper extends Object {

	/**
	 * @param uri
	 * @param activity
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static byte[] getThumbnailDataGivenParentFilePath(
			final String filePath, final Activity activity)
			throws FileNotFoundException, IOException {

		byte[] thumbnailData = null;
		final Bitmap bmp = BitmapFactory.decodeResource(
				activity.getResources(), R.drawable.film_strip);
		thumbnailData = ImageHelper.getDataFromBmp(bmp);

		return thumbnailData;
	}

}
