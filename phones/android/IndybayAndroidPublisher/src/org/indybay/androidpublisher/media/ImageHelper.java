/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.media;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.indybay.androidpublisher.persistence.AndroidFileHelper;
import org.indybay.publishhelper.persistence.FileHelper;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Android specific image manipulation methods
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class ImageHelper extends Object {

	/**
	 * Creates a bitmap given data
	 * 
	 * @param data
	 * @return bitmap
	 */
	public static Bitmap getBmpFromData(final byte[] data) {
		if (data == null) {
			// create a blank bitmap if there is a bug
			return Bitmap.createBitmap(200, 200, Bitmap.Config.ALPHA_8);
		}
		final Bitmap imageBmp = BitmapFactory.decodeByteArray(data, 0,
				data.length);
		return imageBmp;
	}

	/**
	 * Given a Bitmap object, returns the bytes of a jpeg version of it
	 * 
	 * @param bitMap
	 * @return byte array
	 * @throws IOException
	 */
	public static byte[] getDataFromBmp(final Bitmap bitMap) throws IOException {
		final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bitMap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
		final int size = bytes.size();
		final ByteArrayInputStream fileInputStream = new ByteArrayInputStream(
				bytes.toByteArray());
		final byte[] photoData = new byte[size];
		fileInputStream.read(photoData);
		return photoData;
	}

	/**
	 * Tries to find an existing thumbnail, or creates a new one for a given Uri
	 * of an image
	 * 
	 * @param photoUri
	 * @param resolver
	 * @return thumbnail bytes
	 * @throws IOException
	 */
	public static byte[] getThumbnailDataGivenParentFilePath(
			final String filePath, final ContentResolver resolver)
			throws IOException {

		Cursor cursor = null;
		byte[] photoData = null;
		final Uri photoUri = ImageHelper.getUriFromFilePath(filePath);
		try {

			final String[] projection = new String[] {

			android.provider.MediaStore.Images.Thumbnails.DATA };
			Log.i("ImageHelper", "photoURI=" + photoUri.toString());
			final long imageId = ImageHelper.getIdFromUri(photoUri, resolver);

			Log.i("ImageHelper", "imageId=" + imageId);

			if (imageId != 0) {
				cursor = android.provider.MediaStore.Images.Thumbnails
						.queryMiniThumbnail(
								resolver,
								imageId,// photoUri,
								android.provider.MediaStore.Images.Thumbnails.MINI_KIND,
								projection);

				if ((cursor != null) && cursor.moveToFirst()) {
					final FileHelper filehelper = new AndroidFileHelper();
					photoData = filehelper.getDataFromFilePath(cursor
							.getString(0));
					System.gc();
				} else {
					Log.i("AndroidMediaHelper.getThumbnailDataGivenParentUri",
							"thumbnail not found");

				}
				if (cursor != null) {
					cursor.close();
				}
				System.gc();
			}
		} catch (final Exception e) {
			Log.e("ImageHelper", "getThumbnailDataGivenParentUri", e);
		}

		if (photoData == null) {
			Log.i("AndroidMediaHelper.getThumbnailDataGivenParentUri",
					"photoData was null");
			Bitmap bitmap = android.provider.MediaStore.Images.Media.getBitmap(
					resolver, photoUri);
			Bitmap bitmap2 = ImageHelper.resizeBitmapIfNeeded(bitmap, 250);
			bitmap = null;
			System.gc();
			photoData = ImageHelper.getDataFromBmp(bitmap2);
			bitmap2 = null;
			System.gc();
		}
		return photoData;
	}

	/**
	 * Resizes a bitmap gieven a maximum dimension
	 * 
	 * @param bitmap
	 * @param diagonalMaxSize
	 * @param rotate
	 * @return resized bitmap
	 */
	public static Bitmap resizeBitmapIfNeeded(final Bitmap bitmap,
			final int diagonalMaxSize) {

		final int oldWidth = bitmap.getWidth();
		final int oldHeight = bitmap.getHeight();
		final int oldDiagonal = (int) Math.round(Math
				.sqrt((oldWidth * oldWidth) + (oldHeight * oldHeight)));

		final float scaleAmount = ((float) diagonalMaxSize) / oldDiagonal;

		if (scaleAmount > 1) {
			return bitmap;
		}

		// createa matrix for the manipulation
		final Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(scaleAmount, scaleAmount);
		// rotate the Bitmap (not yet supported but leaving this in comment so
		// it can be easier to add)
		// if (rotate == RotateDirection.ROTATE_RIGHT) {
		// matrix.postRotate(90);
		// } else if (rotate == RotateDirection.ROTATE_180) {
		// matrix.postRotate(180);
		// } else if (rotate == RotateDirection.ROTATE_LEFT) {
		// matrix.postRotate(270);
		// }

		// recreate the new Bitmap
		final Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
				oldWidth, oldHeight, matrix, true);
		return resizedBitmap;
	}

	/**
	 * Returns an image id from a Uri
	 * 
	 * @param photoUri
	 * @param resolver
	 * @return id
	 */
	private static long getIdFromUri(final Uri photoUri,
			final ContentResolver resolver) {
		final String[] projection = new String[] { BaseColumns._ID };

		final Cursor cursor = android.provider.MediaStore.Images.Media.query(
				resolver, photoUri,

				projection);
		int id = 0;
		if ((cursor != null) && cursor.moveToFirst()) {
			id = cursor.getInt(0);
		}
		if (cursor != null) {
			cursor.close();
		}
		return id;
	}

	/**
	 * @param filePath
	 * @return
	 */
	private static Uri getUriFromFilePath(final String filePath) {
		final File file = new File(filePath);
		return Uri.fromFile(file);
	}

}
