/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.persistence;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.indybay.publishhelper.persistence.FileHelper;
import org.indybay.publishhelper.persistence.WebHelper;

/**
 * Main class for web communications
 * 
 * @author Zogren for Indybay.org
 */

public class AndroidWebHelper extends WebHelper {

	private HttpURLConnection connection = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.indybay.publishhelper.persistence.WebHelper#cleanupAfterPOST()
	 */
	@Override
	protected void cleanup() throws IOException {
		if (this.connection != null) {
			this.connection.disconnect();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.indybay.publishhelper.persistence.WebHelper#getFileHelper()
	 */
	@Override
	protected FileHelper getFileHelper() {
		return new AndroidFileHelper();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.indybay.publishhelper.persistence.WebHelper#getPOSTInputStream()
	 */
	@Override
	protected InputStream getInputStream() throws IOException {
		return this.connection.getInputStream();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.publishhelper.persistence.WebHelper#getPOSTOutputStream()
	 */
	@Override
	protected OutputStream getOutputStream() throws IOException {
		final OutputStream out = this.connection.getOutputStream();
		final DataOutputStream os = new DataOutputStream(out);
		return os;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.publishhelper.persistence.WebHelper#setupPOSTConnectionFromURL
	 * ()
	 */
	@Override
	protected void setupGETConnectionFromURL(final String url)
			throws IOException {
		final URL urlObj = new URL(url);
		this.connection = (HttpURLConnection) urlObj.openConnection();
		this.connection.setUseCaches(false);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.publishhelper.persistence.WebHelper#setupPOSTConnectionFromURL
	 * ()
	 */
	@Override
	protected void setupPOSTConnectionFromURL(final String url)
			throws IOException {
		final URL urlObj = new URL(url);
		this.connection = (HttpURLConnection) urlObj.openConnection();
		this.connection.setUseCaches(false);
		this.connection.setDoInput(true);
		this.connection.setDoOutput(true);
		this.connection.setUseCaches(false);
		this.connection.setRequestMethod("POST");
		this.connection.setRequestProperty("Content-Type",
				"multipart/form-data;  boundary="
						+ WebHelper.HTTP_POST_BOUNDARY);

	}

}
