/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.indybay.publishhelper.persistence.FileHelper;

/**
 * @author Zogren
 * 
 */
public class AndroidFileHelper extends FileHelper {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.java.controller.WebHelper#getInputStreamFromFilePath(java
	 * .lang.String)
	 */
	@Override
	protected InputStream getInputStreamFromFilePath(final String filePath)
			throws IOException {

		if (filePath == null) {
			throw new IOException("No Content In Atachment");
		}

		final File fc = new File(filePath);

		if (!fc.exists()) {
			throw new IOException("Invalid File Name " + filePath);
		}

		final InputStream fis = new FileInputStream(fc);
		return fis;

	}

}
