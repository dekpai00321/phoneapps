/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.model;

import org.indybay.publishhelper.model.IndyPreferences;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Additional methods added on top of IndyPrerences. For now this is just the
 * save and load which have to be android specific if we want to make sure of
 * the SharedPreferences object
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class AndroidPreferences extends IndyPreferences {

	/**
	 * context needed so we can access shared preferences
	 */
	private Activity context = null;

	/**
	 * Constructor sets the context as well as the file path where newsitems
	 * should be cached (by making use of the cachedir folder for the Activity)
	 * 
	 * @param context
	 */
	public AndroidPreferences(final Activity context) {
		this.context = context;
		this.load();
	}

	/**
	 * Loads saved preferences
	 */
	public void load() {
		final SharedPreferences settings = this.context.getSharedPreferences(
				IndyPreferences.PREFS_NAME, 0);
		this.setBaseURL(settings.getString("base_url", this.getBaseURL()));
		this.setPersonalKey(settings.getString("personal_key", this
				.getPersonalKey()));

		this.setDefaultAuthor(settings.getString("default_author", this
				.getDefaultAuthor()));
		this.setDefaultTitle(settings.getString("default_title", this
				.getDefaultTitle()));
		this.setDefaultSummary(settings.getString("default_summary", this
				.getDefaultSummary()));
		this.setDefaultText(settings.getString("default_text", this
				.getDefaultText()));

		this.setResetAuthor(settings.getBoolean("reset_author", this
				.isResetAuthor()));
		this.setResetTitle(settings.getBoolean("reset_title", this
				.isResetTitle()));
		this.setResetSummary(settings.getBoolean("reset_summary", this
				.isResetSummary()));
		this
				.setResetText(settings.getBoolean("reset_text", this
						.isResetText()));

		this.setMaxFilesAllowed(settings.getInt("max_attachments", this
				.getMaxFilesAllowed()));
		this.setMaxUploadSizeAllowed(settings.getInt("max_upload_size", this
				.getMaxUploadSizeAllowed()));
	}

	/**
	 * Save preferences to disk
	 */
	public void save() {
		final SharedPreferences settings = this.context.getSharedPreferences(
				IndyPreferences.PREFS_NAME, 0);
		final Editor editor = settings.edit();
		editor.putString("base_url", this.getBaseURL());
		editor.putString("personal_key", this.getPersonalKey());

		editor.putString("default_author", this.getDefaultAuthor());
		editor.putString("default_text", this.getDefaultText());
		editor.putString("default_summary", this.getDefaultSummary());
		editor.putString("default_title", this.getDefaultTitle());

		editor.putBoolean("reset_author", this.isResetAuthor());
		editor.putBoolean("reset_title", this.isResetTitle());
		editor.putBoolean("reset_summary", this.isResetSummary());
		editor.putBoolean("reset_text", this.isResetText());

		editor.putInt("max_attachments", this.getMaxFilesAllowed());
		editor.putInt("max_upload_size", this.getMaxUploadSizeAllowed());

		editor.commit();
	}

}
