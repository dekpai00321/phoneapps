/**
    Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.viewcontroller;

import java.io.IOException;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.MIDlet;

import org.indybay.j2me.persistence.JavaMENewsItemPersister;
import org.indybay.publishhelper.model.IndyPreferences;
import org.indybay.publishhelper.model.NewsItem;

/**
 * @author Zogren for Indybay.org
 */
public class IndybayPublisherMIDlet extends MIDlet {

	public NewsItem newsItem = null;
	/**
     *
     */
	public IndyPreferences preferences = new IndyPreferences();
	private AttachmentListPage attachmentListPage;
	private Form currentForm;
	private FileChooser fileChooser;
	private boolean firstTime;
	private PublishPage publishPage;

	public IndybayPublisherMIDlet() {
		this.firstTime = true;
	}

	public void destroyApp(final boolean unconditional) {
	}

	public void displayError(String message) {
		if (message == null) {
			message = "Error";
		}
		final Alert splashScreen = new Alert(null, message, null,
				AlertType.WARNING);
		splashScreen.setTimeout(3000);
		Display.getDisplay(this).setCurrent(splashScreen);
	}

	public void displayInfo(String message) {
		if (message == null) {
			message = "Error";
		}
		final Alert splashScreen = new Alert(null, message, null,
				AlertType.INFO);
		// splashScreen.setTimeout(3000);
		Display.getDisplay(this).setCurrent(splashScreen);
	}

	public void pauseApp() {
	}

	public void startApp() {
		if (this.firstTime) {
			this.publishPage = new PublishPage(this);
			this.fileChooser = new FileChooser(this);
			this.attachmentListPage = new AttachmentListPage(this);

			this.firstTime = false;
		}

		this.switchToPublishPage();
	}

	public void switchToFileChooser() {
		this.currentForm = this.fileChooser;
		// this.currentForm.setCommandListener(this.fileChooser);
		Display.getDisplay(this).setCurrent(this.currentForm);
		this.fileChooser.initForm();
	}

	public void switchToPublishPage() {
		this.currentForm = this.publishPage;
		this.currentForm.setCommandListener(this.publishPage);
		this.publishPage.refreshForm();
		Display.getDisplay(this).setCurrent(this.currentForm);

	}

	void resetNewsItem() {
		this.newsItem.reset(this.preferences);
	}

	void saveNewsItem() throws IOException {
		final JavaMENewsItemPersister persister = new JavaMENewsItemPersister();
		persister.save(this.newsItem);
	}

	void switchToFileManager() {
		this.currentForm = this.attachmentListPage;
		this.currentForm.setCommandListener(this.attachmentListPage);
		this.attachmentListPage.refreshForm();
		Display.getDisplay(this).setCurrent(this.currentForm);
	}
}
