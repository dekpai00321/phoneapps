/**
    Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.viewcontroller;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

import org.indybay.j2me.persistence.JavaMENewsItemPersister;

/**
 * @author Zogren for Indybay.org
 */
public abstract class IndyPublisherPage extends Form implements CommandListener {

	/**
     *
     */
	protected Command addAttachmentCommand = new Command("Add Attachment",
			Command.OK, 1);
	/**
     *
     */
	protected final Command addAttachmentsCommand = new Command(
			"Add Attachment", Command.OK, 1);
	/**
     *
     */
	protected final Command exitCommand = new Command("Exit", Command.EXIT, 1);
	/**
     *
     */
	protected Command listAttachmentsCommand = new Command(
			"Manage Attachments", Command.OK, 1);
	/**
     *
     */
	protected final Command manageAttachmentsCommand = new Command(
			"Manage Attachments", Command.OK, 1);
	/**
     *
     */
	protected IndybayPublisherMIDlet parentMidlet;
	/**
     *
     */
	protected final Command publishPageCommand = new Command("Publish Page",
			Command.OK, 1);
	/**
     *
     */
	protected Command saveCommand = new Command("Save", Command.OK, 1);

	/**
	 * @param newParent
	 */
	public IndyPublisherPage(final IndybayPublisherMIDlet newParent,
			final String additionalName) {

		super("Indybay.org: " + additionalName);

		this.parentMidlet = newParent;
		if (this.parentMidlet.newsItem == null) {

			final JavaMENewsItemPersister persister = new JavaMENewsItemPersister();
			this.parentMidlet.newsItem = persister
					.loadFromFileOrInitFromPrefs(this.parentMidlet.preferences);

		}

		this.setCommandListener(this);

	}

	public void addCommonCommands(final Command currentPageCommand) {
		if (currentPageCommand != this.publishPageCommand) {
			this.addCommand(this.publishPageCommand);
		}
		if (currentPageCommand != this.addAttachmentCommand) {
			this.addCommand(this.addAttachmentCommand);
		}
		if (currentPageCommand != this.listAttachmentsCommand) {
			this.addCommand(this.listAttachmentsCommand);
		}
		if (currentPageCommand != this.saveCommand) {
			this.addCommand(this.saveCommand);
		}
		if (currentPageCommand != this.exitCommand) {
			this.addCommand(this.exitCommand);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.microedition.lcdui.CommandListener#commandAction(javax.microedition
	 * .lcdui.Command, javax.microedition.lcdui.Displayable)
	 */
	public void commandAction(final Command c, final Displayable d) {
		this.refreshNewsItem();
		try {
			this.parentMidlet.saveNewsItem();
		} catch (final Exception e) {
			e.printStackTrace();
		}
		if (c == this.addAttachmentCommand) {
			this.parentMidlet.switchToFileChooser();
		} else if (c == this.saveCommand) {
			// all actions part of common actions for now
		} else if (c == this.exitCommand) {
			this.parentMidlet.destroyApp(false);
			this.parentMidlet.notifyDestroyed();
		} else if (c == this.saveCommand) {
			// all actions part of common actions for now
		} else if (c == this.listAttachmentsCommand) {
			this.parentMidlet.switchToFileManager();
		} else if (c == this.publishPageCommand) {
			this.parentMidlet.switchToPublishPage();
		}

	}

	/**
     *
     */
	protected abstract void refreshForm();

	/**
     *
     */
	protected abstract void refreshNewsItem();
}
