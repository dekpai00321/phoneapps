/**
    Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.viewcontroller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemRegistry;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;

import org.indybay.j2me.controller.JavaMEWebHelper;
import org.indybay.j2me.media.JavaMEImageHelper;
import org.indybay.publishhelper.media.MediaHelper;
import org.indybay.publishhelper.model.NewsItemAttachment;
import org.indybay.publishhelper.persistence.WebHelper;

/**
 * @author Zogren for Indybay.org
 */
public class FileChooser extends IndyPublisherPage implements CommandListener {
	/**
	 * 
	 */
	private final static String MEGA_ROOT = "/";
	/**
	 * 
	 */
	private final static char SEP = '/';

	/**
	 * 
	 */
	private final static String SEP_STR = "/";
	/**
	 * 
	 */
	private final static String UP_DIRECTORY = "..";
	/**
	 * 
	 */
	private final Command back = new Command("Back", Command.BACK, 2);

	/**
	 * 
	 */
	private final Command cancel = new Command("Cancel", Command.EXIT, 3);

	/**
		 * 
		 */
	private String currDirName;

	/**
	 * 
	 */
	private String currFileName = null;
	private final Command select = new Command("Select", Command.OK, 2);

	private final Command testDownload = new Command("TestDownload",
			Command.OK, 2);
	/**
	 * 
	 */
	private final Command view = new Command("View", Command.ITEM, 1);

	/**
	 * @param newParent
	 */
	public FileChooser(final IndybayPublisherMIDlet newParent) {
		super(newParent, "Choose A file");
	}

	/**
	 * @param c
	 * @param d
	 */
	public void commandAction(final Command c, final Displayable d) {
		if (c == this.view) {
			final List curr = (List) d;
			final String currFile = curr.getString(curr.getSelectedIndex());
			new Thread(new Runnable() {
				public void run() {
					if (currFile.endsWith(FileChooser.SEP_STR)
							|| currFile.equals(FileChooser.UP_DIRECTORY)) {
						FileChooser.this.traverseDirectory(currFile);
					} else {
						FileChooser.this.showFile(currFile);
					}
				}
			}).start();
		} else if (c == this.back) {
			this.showCurrDir();
		} else if (c == this.select) {
			final NewsItemAttachment newAttachment = new NewsItemAttachment();
			newAttachment.setFilePath("file://localhost/" + this.currDirName
					+ this.currFileName);
			this.parentMidlet.newsItem.getAttachments().addElement(
					newAttachment);
			this.parentMidlet.switchToPublishPage();

		} else if (c == this.cancel) {
			this.parentMidlet.switchToPublishPage();
		} else if (c == this.testDownload) {
			this.testDownload();
		} else {
			super.commandAction(c, d);
		}

	}

	/**
	 * 
	 */
	public void initForm() {
		this.currDirName = FileChooser.MEGA_ROOT;
		boolean isAPIAvailable = false;
		if (System.getProperty("microedition.io.file.FileConnection.version") != null) {
			isAPIAvailable = true;
			try {
				this.showCurrDir();
			} catch (final SecurityException e) {
				e.printStackTrace();
			} catch (final Exception e) {
				e.printStackTrace();
			}

		} else {
			final StringBuffer splashText = new StringBuffer(this.parentMidlet
					.getAppProperty("MIDlet-Name")).append("\n").append(
					this.parentMidlet.getAppProperty("MIDlet-Vendor")).append(
					isAPIAvailable ? "" : "\nFileConnection API not available");
			final Alert splashScreen = new Alert(null, splashText.toString(),
					null, AlertType.INFO);
			splashScreen.setTimeout(3000);
			Display.getDisplay(this.parentMidlet).setCurrent(splashScreen);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.j2me.viewcontroller.IndyPublisherPage#refreshFormFromNewsItem
	 * ()
	 */
	public void refreshForm() {
		// does nothing for now
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.j2me.viewcontroller.IndyPublisherPage#refreshNewsItemFromForm
	 * ()
	 */
	public void refreshNewsItem() {
		// does nothing for now
	}

	/**
     * 
     */
	private void testDownload() {
		final WebHelper webHelper = new JavaMEWebHelper();
		try {
			FileConnection fc = (FileConnection) Connector
					.open("file://localhost/" + this.currDirName + "test.png");
			if (fc.exists()) {
				fc.delete();
			}
			fc.create();
			OutputStream os = fc.openOutputStream();
			webHelper
					.httpBinaryGET(
							"http://upload.wikimedia.org/wikipedia/commons/3/37/KTurtle_-_Example_2.png",
							os);

			os.close();

			fc = (FileConnection) Connector.open("file://localhost/"
					+ this.currDirName + "region_map_v2.gif");
			if (fc.exists()) {
				fc.delete();
			}
			fc.create();
			os = fc.openOutputStream();
			webHelper
					.httpBinaryGET("http://127.0.0.1/im/region_map_v2.gif", os);

			os.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	void showCurrDir() {
		Enumeration e;
		FileConnection currDir = null;
		List browser;
		try {
			if (FileChooser.MEGA_ROOT.equals(this.currDirName)) {
				e = FileSystemRegistry.listRoots();

				browser = new List(this.currDirName, Choice.IMPLICIT);
			} else {
				currDir = (FileConnection) Connector.open("file://localhost/"
						+ this.currDirName);
				e = currDir.list();

				browser = new List(this.currDirName, Choice.IMPLICIT);
				browser.append(FileChooser.UP_DIRECTORY, null);
			}
			while (e.hasMoreElements()) {
				final String fileName = (String) e.nextElement();
				if (fileName.charAt(fileName.length() - 1) == FileChooser.SEP) {
					browser.append(fileName, null);
				} else {
					browser.append(fileName, null);
				}
			}
			browser.setSelectCommand(this.view);
			browser.addCommand(this.cancel);
			browser.addCommand(this.testDownload);
			browser.setCommandListener(this);
			if (currDir != null) {
				currDir.close();
			}
			Display.getDisplay(this.parentMidlet).setCurrent(browser);
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * @param fileName
	 */
	void showFile(final String fileName) {
		final JavaMEImageHelper imageHelper = new JavaMEImageHelper();
		Displayable item = null;
		try {
			this.currFileName = fileName;
			final FileConnection fc = (FileConnection) Connector
					.open("file://localhost/" + this.currDirName + fileName);
			if (!fc.exists()) {
				throw new IOException("File does not exists");
			}
			InputStream fis = fc.openInputStream();
			final int type = imageHelper.getMediaTypeFromFileName(fileName);

			if (type == MediaHelper.MEDIA_TYPE_IMAGE) {
				item = new Form("Image: " + fileName);
				Image image = null;
				try {
					image = Image.createImage(fis);

					((Form) item).append(image);
				} catch (final Exception e) {
					e.printStackTrace();
					try {
						fis.close();
						fis = fc.openInputStream();

					} catch (final Exception ee) {
						ee.printStackTrace();
					}
				}
				if (image == null) {
					final StringItem stringItem = new StringItem("",
							"Unable To Display Image Type On This Phone");
					((Form) item).append(stringItem);
				}
			} else {
				final byte[] b = new byte[1024];
				final int length = fis.read(b, 0, 1024);

				item = new TextBox("Text File: " + fileName, null, 1024,
						TextField.ANY | TextField.UNEDITABLE);
				if (length > 0) {
					((TextBox) item).setString(new String(b, 0, length));
				}
			}
			fis.close();
			fc.close();
			item.addCommand(this.select);
			item.addCommand(this.back);

			item.setCommandListener(this);

			Display.getDisplay(this.parentMidlet).setCurrent(item);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param fileName
	 */
	void traverseDirectory(final String fileName) {
		if (this.currDirName.equals(FileChooser.MEGA_ROOT)) {
			if (fileName.equals(FileChooser.UP_DIRECTORY)) {
				// can not go up from MEGA_ROOT
				return;
			}
			this.currDirName = fileName;
		} else if (fileName.equals(FileChooser.UP_DIRECTORY)) {
			// Go up one directory
			// TODO use setFileConnection when implemented
			final int i = this.currDirName.lastIndexOf(FileChooser.SEP,
					this.currDirName.length() - 2);
			if (i != -1) {
				this.currDirName = this.currDirName.substring(0, i + 1);
			} else {
				this.currDirName = FileChooser.MEGA_ROOT;
			}
		} else {
			this.currDirName = this.currDirName + fileName;
		}
		this.showCurrDir();
	}
}
