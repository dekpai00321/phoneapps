/**
	Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.persistence;

import java.io.IOException;

import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;

import org.indybay.publishhelper.model.NewsItem;
import org.indybay.publishhelper.persistence.NewsItemPersister;

/**
 * @author Zogren
 * 
 */
public class JavaMENewsItemPersister extends NewsItemPersister {

	/**
     *
     */
	private final String recordStoreName = "IndyPublisher";

	/**
	 * Sets up a news item by either loading it from a cached file or setting up
	 * a new one with default values
	 * 
	 * @param filePath
	 * @param preferences
	 * @return newsItem
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public NewsItem load() throws IOException {

		NewsItem newsItem = null;
		RecordStore rs = null;
		try {
			rs = RecordStore.openRecordStore(this.recordStoreName, true);

			if (rs.getNumRecords() > 0) {

				final RecordEnumeration re = rs.enumerateRecords(null, null,
						false);

				if (re.hasNextElement()) {
					final byte[] data = re.nextRecord();
					final NewsItem newNewsItem = new NewsItem();
					final String str = new String(data);

					newNewsItem.loadFromString(str);
					newsItem = newNewsItem;

				}
			}
		} catch (final Exception ee) {
			ee.printStackTrace();

		} finally {
			if (rs != null) {
				try {
					rs.closeRecordStore();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		}
		return newsItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.java.persistence.NewsItemPersister#save(org.indybay.java.
	 * model.NewsItem)
	 */
	public void save(final NewsItem newsItem) throws IOException {

		RecordStore rs = null;

		try {
			rs = RecordStore.openRecordStore(this.recordStoreName, true);

			final RecordEnumeration re = rs.enumerateRecords(null, null, false);
			int nextId = 0;
			while (re.hasNextElement()) {
				nextId = re.nextRecordId();

				rs.deleteRecord(nextId);
			}

			final String newsItemAsString = newsItem.toString();

			final byte[] bytes = newsItemAsString.getBytes();
			rs.addRecord(bytes, 0, bytes.length);

		} catch (final Exception ee) {
			ee.printStackTrace();

		} finally {
			if (rs != null) {
				try {
					rs.closeRecordStore();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		}

	}
}
