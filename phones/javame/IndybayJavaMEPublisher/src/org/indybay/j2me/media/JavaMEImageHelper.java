/**
    Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device

    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.media;

import javax.microedition.lcdui.Image;

import org.indybay.publishhelper.media.MediaHelper;

/**
 * 
 * @author zogen
 */
public class JavaMEImageHelper extends MediaHelper {

	/**
	 * Changes size of bitmap.
	 * 
	 * @param sourceImage
	 *            source image.
	 * @param newWidth
	 *            width of new image.
	 * @param newHeight
	 *            height of new image.
	 * @return scaled image.
	 */
	public Image scaleImage(final Image sourceImage, final double magnification) {
		// Remember image size.

		final int oldWidth = sourceImage.getWidth();
		final int oldHeight = sourceImage.getHeight();
		final int newWidth = (int) Math.floor(oldWidth * magnification);
		final int newHeight = (int) Math.floor(oldHeight * magnification);

		// Create buffer for input image.
		final int[] inputData = new int[oldWidth * oldHeight];
		// Fill it with image data.
		sourceImage.getRGB(inputData, 0, oldWidth, 0, 0, oldWidth, oldHeight);

		// Create buffer for output image.
		final int[] outputData = new int[newWidth * newHeight];

		final int YD = (oldHeight / newHeight - 1) * oldWidth;
		final int YR = oldHeight % newHeight;
		final int XD = oldWidth / newWidth;
		final int XR = oldWidth % newWidth;
		// New image buffer offset.
		int outOffset = 0;
		// Source image buffer offset.
		int inOffset = 0;

		for (int y = newHeight, YE = 0; y > 0; y--) {
			for (int x = newWidth, XE = 0; x > 0; x--) {
				// Copying pixel from old image to new.
				outputData[outOffset++] = inputData[inOffset];
				inOffset += XD;
				// Calculations for "smooth" scaling in x dimension.
				XE += XR;
				if (XE >= newWidth) {
					XE -= newWidth;
					inOffset++;
				}
			}
			inOffset += YD;
			// Calculations for "smooth" scaling in y dimension.
			YE += YR;
			if (YE >= newHeight) {
				YE -= newHeight;
				inOffset += oldWidth;
			}
		}
		// Create image from output buffer.
		return Image.createRGBImage(outputData, newWidth, newHeight, false);
	}

}
