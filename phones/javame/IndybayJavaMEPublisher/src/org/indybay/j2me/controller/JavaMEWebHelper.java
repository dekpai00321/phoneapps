/**
    Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.controller;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import org.indybay.publishhelper.persistence.FileHelper;
import org.indybay.publishhelper.persistence.WebHelper;

/**
 * Main class for web communications
 * 
 * @author Zogren for Indybay.org
 */
public class JavaMEWebHelper extends WebHelper {

	private HttpConnection connection = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.indybay.publishhelper.persistence.WebHelper#cleanup()
	 */
	protected void cleanup() throws IOException {
		if (this.connection != null) {
			this.connection.close();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.indybay.publishhelper.persistence.WebHelper#getFileHelper()
	 */
	protected FileHelper getFileHelper() {
		return new JavaMEFileHelper();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.indybay.publishhelper.persistence.WebHelper#getInputStream()
	 */
	protected InputStream getInputStream() throws IOException {
		final DataInputStream in = this.connection.openDataInputStream();
		return in;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.indybay.publishhelper.persistence.WebHelper#getOutputStream()
	 */
	protected OutputStream getOutputStream() throws IOException {
		final OutputStream os = this.connection.openOutputStream();
		return os;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.publishhelper.persistence.WebHelper#setupGETConnectionFromURL
	 * (java.lang.String)
	 */
	protected void setupGETConnectionFromURL(final String url)
			throws IOException {
		this.connection = (HttpConnection) Connector.open(url);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.publishhelper.persistence.WebHelper#setupPOSTConnectionFromURL
	 * (java.lang.String)
	 */
	protected void setupPOSTConnectionFromURL(final String url)
			throws IOException {
		this.connection = (HttpConnection) Connector.open(url);
		this.connection.setRequestMethod("POST");
		this.connection.setRequestProperty("Content-Type",
				"multipart/form-data;  boundary="
						+ WebHelper.HTTP_POST_BOUNDARY);

	}

}
